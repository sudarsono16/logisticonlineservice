package com.logol.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;

//import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.google.gson.Gson;
import java.lang.reflect.Type;
import java.net.URL;

import com.google.gson.reflect.TypeToken;

import adapter.APIAdapter;
import adapter.ApkVersionAdapter;
import adapter.CodecoAdapter;
import adapter.ConvertDateTimeHelper;
import adapter.DailyNewsAdapter;
import adapter.DebitNoteAdapter;
import adapter.DriverAdapter;
import adapter.DriverImageAdapter;
import adapter.EmailAdapter;
import adapter.KojaAdapter;
import adapter.LiveTrackingAdapter;
import adapter.LogAdapter;
import adapter.LogBillingDepoAdapter;
import adapter.TokenAdapter;
import adapter.UserConfigAdapter;
import adapter.ValidateNull;
import adapter.VendorOrderDetailAdapter;
import adapter.WriteFileAdapter;
import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlDebitNote;
import model.mdlDebitNoteDetail;
//import net.sourceforge.jtds.jdbc.MSSqlServerInfo;
import model.mdlDriverImage;
import model.mdlLogBillingDepo;
import model.mdlSettingInvoice;
import model.mdlCodeco;

@RestController
public class controller {
	final static Logger logger = Logger.getLogger(controller.class);

	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody String GetPing()
	{
		// untuk generate grand total seluruh debit note
//		List<model.mdlDebitNote> LoadDebitNote = DebitNoteAdapter.LoadDebitNote();
//		for (mdlDebitNote debitNote : LoadDebitNote) {
//			int amount = DebitNoteAdapter.amountInvoice(debitNote.debitNoteNumber, "mobile");
//			DebitNoteAdapter.updateFinalRateByDebitNoteNumberDetailID(debitNote.debitNoteNumber, amount, "mobile");
//			System.out.println(debitNote.debitNoteNumber);
//		}
		
		// test create debitnote detail
		List<model.mdlDebitNote> LoadDebitNote = DebitNoteAdapter.LoadDebitNote();
		for (mdlDebitNote debitNote : LoadDebitNote) {
			List<mdlDebitNoteDetail> dnDetail = DebitNoteAdapter.getSetingInvoiceByOrderManagementID(debitNote.orderManagementID, debitNote.debitNoteNumber, "admin");
			for (mdlDebitNoteDetail dnDetail2 : dnDetail) {
				System.out.println(dnDetail2.getDebitNoteDetailID());
				DebitNoteAdapter.insertDebitNoteDetail(dnDetail2, "kkk");
			}
		}
		
		
		// test create invoice
//		mdlSettingInvoice settingInvoice = DebitNoteAdapter.getSetingInvoiceByCusID("LG/201908/E0001", "mobile");
//		model.mdlDebitNote debitNote = new model.mdlDebitNote();
//		/*Double customerRate = rowset.getDouble(16);*/
//		debitNote.customerID = "CUST-20190220-0002";
//		debitNote.orderManagementID = "LG/201908/E0001";
//		debitNote.date = LocalDateTime.now().toString();
//		debitNote.amount = 	2222;
//		debitNote.status = "Unpaid";
//		debitNote.finalRate = settingInvoice.getFinalRate();
//		debitNote.customClearance = settingInvoice.getCustomClearance();
//		debitNote.ppn = settingInvoice.getPpn();
//		debitNote.pph23 = settingInvoice.getPph23();
//		System.out.println(debitNote.amount);
//		DebitNoteAdapter.InsertDebitNote(debitNote);
//		
//		List<mdlDebitNoteDetail> dnDetail = DebitNoteAdapter.getSetingInvoiceByOrderManagementID(debitNote.orderManagementID, debitNote.debitNoteNumber, "mobile");
//		for (mdlDebitNoteDetail dnDetail2 : dnDetail) {
//			DebitNoteAdapter.insertDebitNoteDetail(dnDetail2, "mobile");
//		}
//		
//		int amount = DebitNoteAdapter.amountInvoice(debitNote.debitNoteNumber, "mobile");
//		System.out.println(amount);
//		DebitNoteAdapter.updateFinalRateByDebitNoteNumberDetailID(debitNote.debitNoteNumber, amount, "mobile");
		
		
		// List<mdlDebitNoteDetail> dnDetail = DebitNoteAdapter.getSetingInvoiceByOrderManagementID("LG/201907/E0126", "DN/EX/2019/07/0076", "admin");
		
//		EmailAdapter.SendEmailDeliveredToCustomer("VODT-20190410-000001");
//		VendorOrderDetailAdapter.CheckOrderStatus();
//		System.out.println("berhasil");
		return "berhasil";
	}
	
	@RequestMapping(value = "/getAssignment/{id}",method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetAssignment(@PathVariable("id") String driverID, 
														@RequestHeader("Authorization") String Authorization,
														@RequestHeader("DriverID") String DriverID,
														HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "GetAssignment";
		Gson gson = new Gson();
		String jsonIn = driverID;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (driverID == null || driverID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Driver ID tidak valid";
			return mdlResult;
		}
		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				Context context = (Context)new InitialContext().lookup("java:comp/env");
				
				//get the base path for client(mobile) access from web.xml
		    	String smsContact = (String)context.lookup("param_sms_contact");
				List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
				listVendorOrderDetail = VendorOrderDetailAdapter.VendorOrderDetailLoadByDriverID(driverID, smsContact);

				if(!listVendorOrderDetail.isEmpty() )
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = listVendorOrderDetail;
					String jsonOut = gson.toJson(mdlResult);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="02";
					mdlResult.StatusMessage = "Tidak ada assignment";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}
	
	@RequestMapping(value = "/getIsoCode/{containerNumber}",method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetIsoCode(@PathVariable("containerNumber") String containerNumber, 
														@RequestHeader("Authorization") String Authorization,
														@RequestHeader("DriverID") String DriverID,
														HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "GetIsoCode";
		Gson gson = new Gson();
		String jsonIn = containerNumber;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (containerNumber == null || containerNumber.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Nomor container kosong";
			return mdlResult;
		}
		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				String isoCode = "";
				isoCode = VendorOrderDetailAdapter.LoadIsoCodeByContainer(containerNumber, DriverID);

				if(!isoCode.equals("") )
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = isoCode;
					String jsonOut = gson.toJson(mdlResult);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="02";
					mdlResult.StatusMessage = "Tidak ada iso code";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}

	@RequestMapping(value="/updateOrderStatus", method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlResult UpdateOrderStatus(@RequestBody model.mdlUpdateVendorOrderDetailStatus param, 
			@RequestHeader("Authorization") String Authorization,
			@RequestHeader("DriverID") String DriverID,
			HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "UpdateOrderStatus";
		Gson gson = new Gson();
		String jsonIn = gson.toJson(param);
		Boolean success = false;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (param.vendorOrderDetailID == null || param.vendorOrderDetailID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "ID tidak valid";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			return mdlResult;
		}else if (param.vendorDetailStatus == null || param.vendorDetailStatus.equals("")){
			mdlResult.StatusCode = "02";
			mdlResult.StatusMessage = "Status tidak valid";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			return mdlResult;
		}

		try{
			// update truck masuk on depo, keluar depo, masuk pabrik, keluar pablik, masuk pelabukan, keluar plabujan
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				// pengecekan dikoja
				if(param.portID != null && (param.portID.equals("PORT-0002") && param.vendorDetailStatus == 3)){
					//kondisi ketika gatein port ke koja, lakuin ini
					//codingan untuk manggil servis get cms ke koja
					/*Context context = (Context)new InitialContext().lookup("java:comp/env");
					param.kojaUrl = (String)context.lookup("asso_url");
					param.kojaUsername = (String)context.lookup("asso_username");
					param.kojaPassword = (String)context.lookup("asso_password");
					
					Boolean validateCheckIn = APIAdapter.KojaGetCms(param, DriverID);*/
					
					//check cms availability
					Boolean validateCheckIn = false;
					modelKojaGetCms.mdlGetCms cmsData = KojaAdapter.LoadKojaCmsData(param.vendorOrderDetailID, DriverID);
					if(cmsData.cmsLink != null || !cmsData.cmsLink.equals(""))
						validateCheckIn = true;
					
					if(!validateCheckIn){
						mdlResult.StatusCode = "05";
						mdlResult.StatusMessage = "Gagal memproses CMS, silahkan Anda tunggu maksimal 10 menit atau menuju 'Post Gate' untuk mendapatkan panduan";
						String jsonOut = gson.toJson(mdlResult);
						logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
						return mdlResult;
					}
				}
				
				//kondisi normal
				success = VendorOrderDetailAdapter.UpdateVendorOrderDetailStatus(param);
				if(success)
				{
					// insert dn dan cn
					VendorOrderDetailAdapter.CheckOrderStatus(param.vendorOrderDetailID);
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = "Success";
					String jsonOut = gson.toJson(mdlResult);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="03";
					mdlResult.StatusMessage = "Gagal update vendor order detail status";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="04";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}

	@RequestMapping(value = "/insertLiveTracking", method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlResult InsertLiveTracking(@RequestBody model.mdlLiveTracking param, 
			@RequestHeader("Authorization") String Authorization,
			@RequestHeader("DriverID") String DriverID,
			HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "InsertLiveTracking";
		Gson gson = new Gson();
		Boolean success = false;
		String jsonIn = gson.toJson(param);
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (param.vendorID == null || param.vendorID.equals("") ){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Vendor ID tidak valid";
			return mdlResult;
		}else if (param.driverID == null || param.driverID.equals("") ){
			mdlResult.StatusCode = "02";
			mdlResult.StatusMessage = "Driver ID tidak valid";
			return mdlResult;
		}else if (param.vehicleNumber == null || param.vehicleNumber.equals("") ){
			mdlResult.StatusCode = "03";
			mdlResult.StatusMessage = "Vehicle Number tidak valid";
			return mdlResult;
		}

		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				success = LiveTrackingAdapter.InsertLiveTracking(param);

				if(success)
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = "Success";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="04";
					mdlResult.StatusMessage = "Gagal insert live tracking";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="05";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "InsertLiveTracking", jsonIn, param.driverID);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}
	
	@RequestMapping(value = "/insertUpdateIso", method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlResult InsertUpdateIso(@RequestBody model.mdlVendorOrderDetail param, 
			@RequestHeader("Authorization") String Authorization,
			@RequestHeader("DriverID") String DriverID,
			HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "InsertUpdateIso";
		Gson gson = new Gson();
		Boolean success = false;
		String jsonIn = gson.toJson(param);
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (param.containerNumber == null || param.containerNumber.equals("") ){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Nomor Container Kosong";
			return mdlResult;
		}else if (param.isoCode == null || param.isoCode.equals("") ){
			mdlResult.StatusCode = "02";
			mdlResult.StatusMessage = "Iso Code Kosong";
			return mdlResult;
		}else if (param.vendorOrderDetailID == null || param.vendorOrderDetailID.equals("") ){
			mdlResult.StatusCode = "03";
			mdlResult.StatusMessage = "Nomor Order Kosong";
			return mdlResult;
		}

		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				success = VendorOrderDetailAdapter.InsertUpdateIsoCode(param);

				if(success)
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = "Success";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="04";
					mdlResult.StatusMessage = "Gagal insert update iso code";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="05";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "InsertUpdateIso", jsonIn, param.driverID);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}

	@RequestMapping(value = "/getUserConfig/{id}", method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetUserConfig(@PathVariable("id") String deviceID, HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlUserConfig mdlUserConfig = new model.mdlUserConfig();
		String APIName = mdlResult.Title = "GetUserConfig";
		Gson gson = new Gson();
		String jsonIn = deviceID;

		if (deviceID == null || deviceID.equals("") ){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Device ID tidak valid";
			return mdlResult;
		}

		try{
			mdlUserConfig = UserConfigAdapter.GetUserConfig(deviceID);

			if(!(mdlUserConfig.driverID != null))
			{
				mdlResult.StatusCode = "00";
				mdlResult.StatusMessage = "Berhasil";
				mdlResult.Value = mdlUserConfig;
				String jsonOut = gson.toJson(mdlResult);
				logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
			else
			{
				mdlResult.StatusCode ="02";
				mdlResult.StatusMessage = "Device anda belum terdaftar, silahkan hubungi admin";
				String jsonOut = gson.toJson(mdlResult);
				logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "GetUserConfig", jsonIn, deviceID);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}

	@RequestMapping(value = "/getDailyNews/{id}", method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetDailyNews(@PathVariable("id") String driverID, HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		List<model.mdlDailyNews> listDailyNews = new ArrayList<model.mdlDailyNews>();
		String APIName = mdlResult.Title = "GetDailyNews";
		Gson gson = new Gson();
		String jsonIn = driverID;

		if (driverID == null || driverID.equals("") ){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Driver ID tidak valid";
			return mdlResult;
		}

		try{
			listDailyNews = DailyNewsAdapter.GetDailyNews(driverID);

			if(!listDailyNews.isEmpty() )
			{
				mdlResult.StatusCode = "00";
				mdlResult.StatusMessage = "Berhasil";
				mdlResult.Value = listDailyNews;
				String jsonOut = gson.toJson(mdlResult);
				logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
			else
			{
				mdlResult.StatusCode ="02";
				mdlResult.StatusMessage = "Tidak ada daily news untuk driver ini";
				String jsonOut = gson.toJson(mdlResult);
				logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "GetDailyNews", jsonIn, driverID);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlResult Login(@RequestBody model.mdlLogin param, HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlDriverLogin mdlDriverLogin = new model.mdlDriverLogin();
		String APIName = mdlResult.Title = "Login";
		Gson gson = new Gson();
		String jsonIn = param.deviceID;

		if (param.deviceID == null || param.deviceID.equals("") ){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Device ID tidak valid";
			return mdlResult;
		}else if (param.token == null || param.token.equals("") ){
			mdlResult.StatusCode = "02";
			mdlResult.StatusMessage = "Token tidak valid";
			return mdlResult;
		}

		try{
			mdlDriverLogin = DriverAdapter.GetDriverLoginData(param);

			if(mdlDriverLogin.driverID != null )
			{
				mdlResult.StatusCode = "00";
				mdlResult.StatusMessage = "Berhasil";
				mdlResult.Value = mdlDriverLogin;
				String jsonOut = gson.toJson(mdlResult);
				logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
			else
			{
				mdlResult.StatusCode ="03";
				mdlResult.StatusMessage = "Tidak ada data untuk driver ini";
				String jsonOut = gson.toJson(mdlResult);
				logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="04";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "Login", jsonIn, param.deviceID);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}

	@RequestMapping(value = "/loginByUsernamePassword", method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlResult LoginByUsernamePassword(@RequestBody model.mdlLogin param, HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "LoginByUsernamePassword";
		Gson gson = new Gson();
		String jsonIn = gson.toJson(param);
		model.mdlDriverLogin mdlDriverLogin = new model.mdlDriverLogin();

		if (param.username == null || param.username.equals("") ){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Username kosong";
			return mdlResult;
		}
		else if (param.password == null || param.password.equals("") ){
			mdlResult.StatusCode = "02";
			mdlResult.StatusMessage = "Password kosong";
			return mdlResult;
		}
		else if (param.token == null || param.token.equals("") ){
			mdlResult.StatusCode = "03";
			mdlResult.StatusMessage = "Token kosong";
			return mdlResult;
		}

		try{
			Boolean resultUpdateToken = TokenAdapter.UpdateToken(param);
			if(!resultUpdateToken){
				mdlResult.StatusCode = "04";
				mdlResult.StatusMessage = "Update token gagal";
				return mdlResult;
			}
			else{
				mdlDriverLogin = DriverAdapter.CheckLogin(param);

				if(mdlDriverLogin.driverID != null)
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = mdlDriverLogin;
					String jsonOut = gson.toJson(mdlResult);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="05";
					mdlResult.StatusMessage = "Login gagal";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="06";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "Login", jsonIn, param.username);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}
	
	@RequestMapping(value="/updateDriverKmEnd", method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlResult UpdateDriverKmEnd(@RequestBody model.mdlUpdateDriverKmEnd param, 
			@RequestHeader("Authorization") String Authorization,
			@RequestHeader("DriverID") String DriverID,
			HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "UpdateDriverKmEnd";
		Gson gson = new Gson();
		String jsonIn = gson.toJson(param);
		Boolean success = false;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (param.vendorOrderDetailID == null || param.vendorOrderDetailID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "ID tidak valid";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			return mdlResult;
		}else if (param.kmEnd == null || param.kmEnd.equals("")){
			mdlResult.StatusCode = "02";
			mdlResult.StatusMessage = "KM tidak valid";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			return mdlResult;
		}

		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				success = VendorOrderDetailAdapter.UpdateDriverKmEnd(param);

				if(success )
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = "Success";
					String jsonOut = gson.toJson(mdlResult);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="03";
					mdlResult.StatusMessage = "Gagal update KM akhir";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="04";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}
	
	@RequestMapping(value = "/uploadDriverImage", method = RequestMethod.POST,consumes="multipart/form-data")
	public @ResponseBody model.mdlResult uploadDriverImage(@RequestParam(name="file", required=false)  MultipartFile[] Files, 
			@RequestParam(name="signature", required=false)  MultipartFile Signature, 
			@RequestParam(name="json") final String jsonParam,
			@RequestHeader("Authorization") String Authorization,
			@RequestHeader("DriverID") String DriverID)throws  IOException {	
		model.mdlResult Result = new model.mdlResult();
		String APIName = Result.Title = "uploadDriverImage";
		//initiate GSON
		Gson gson = new Gson(); 
		//convert to json parameter
		Type listType = new TypeToken<model.mdlDriverImage>(){}.getType();
		model.mdlDriverImage DriverImageParam = gson.fromJson(jsonParam, listType);
		List<model.mdlDriverImage> listDriverImageParam = new ArrayList<model.mdlDriverImage>();
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;
		try
		{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				// Get the base naming context from web.xml
				Context context = (Context)new InitialContext().lookup("java:comp/env");
				
				//get the base path for client(mobile) access from web.xml
		    	String baseFileAccess = (String)context.lookup("param_file_access");
		    	String baseSeparator = (String)context.lookup("param_separator");
		    	
		    	//initiate variable
		    	String baseFilePath = "",
		    		   paramFileAccess = "",
		    		   paramFilePath = "",
		    		   newFile = "", 
		    		   FileName = "";
		    	InputStream originalFile = null;
		    	
		    	//set the variable for image path's format 1
				baseFilePath = (String)context.lookup("param_file_location");
				Date date = new Date();
				LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				int year  = localDate.getYear();
				String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
				int day   = localDate.getDayOfMonth();
				String driverName = DriverImageParam.driverName;
				String imageType = DriverImageParam.type;
				paramFilePath = baseFilePath + year + baseSeparator + month + baseSeparator + day + baseSeparator + driverName + baseSeparator + imageType + baseSeparator;
				paramFileAccess = baseFileAccess + year + "/" + month + "/" + day + "/" + driverName + "/" + imageType + "/";
							
				//iterate files
				for (int i = 0; i < Files.length; i++) {
					model.mdlDriverImage mdlDriverImage = new model.mdlDriverImage();
					mdlDriverImage.vendorOrderDetailID = DriverImageParam.vendorOrderDetailID;
					mdlDriverImage.driverID = DriverImageParam.driverID;
					mdlDriverImage.type = DriverImageParam.type;
					if(Files[i] != null){
						//get the image name
						FileName = Files[i].getOriginalFilename();
						
						//get the file input stream
						originalFile = Files[i].getInputStream();
						
						//final path of image file in directory
						newFile = paramFilePath  + FileName;
						//final path of image file in database
						mdlDriverImage.path = paramFileAccess + FileName;
						
						//write file
						WriteFileAdapter.Write(originalFile , paramFilePath, newFile);
						
						listDriverImageParam.add(mdlDriverImage);
					}
				}
				
				//write signature image
				if(Signature != null){
					model.mdlDriverImage mdlDriverImage = new model.mdlDriverImage();
					mdlDriverImage.vendorOrderDetailID = DriverImageParam.vendorOrderDetailID;
					mdlDriverImage.driverID = DriverImageParam.driverID;
					mdlDriverImage.type = "SIGNATURE";
					
					//get the image name
					FileName = Signature.getOriginalFilename();
					
					//get the file input stream
					originalFile = Signature.getInputStream();
					
					imageType = "SIGNATURE";
					paramFilePath = baseFilePath + year + baseSeparator + month + baseSeparator + day + baseSeparator + driverName + baseSeparator + imageType + baseSeparator;
					paramFileAccess = baseFileAccess + year + "/" + month + "/" + day + "/" + driverName + "/" + imageType + "/";
					
					//final path of image file in directory
					newFile = paramFilePath  + FileName;
					//final path of image file in database
					mdlDriverImage.path = paramFileAccess + FileName;
					
					//write file
					WriteFileAdapter.Write(originalFile , paramFilePath, newFile);
					
					listDriverImageParam.add(mdlDriverImage);
				}
				
				//insert list driver image to database
				Boolean lResult = DriverImageAdapter.InsertDriverImage(listDriverImageParam);
				
				if (lResult){
					Result.StatusCode = "00";
					Result.StatusMessage = "Berhasil";
					Result.Value = "Success";
					String jsonOut = gson.toJson(Result);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + listDriverImageParam + ", jsonOut:" + jsonOut);
				}
				else
				{
					Result.StatusCode = "01";
					Result.StatusMessage = "Gagal menyimpan data";
					String jsonOut = gson.toJson(Result);
					logger.info("FAILED. API : " + APIName + ", method : POST, jsonIn:" + listDriverImageParam + ", jsonOut:" + jsonOut);
				}
			}
			else{
				Result.StatusCode = "99";
				Result.StatusMessage = "Session habis";
			}
		} catch (Exception e) {
			Result.StatusCode = "04";
			Result.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(Result);
			logger.info("FAILED. API : " + APIName + ", method : POST, jsonIn:" + listDriverImageParam + ", jsonOut:" + jsonOut);
		}

		return Result;
	}
	
	@RequestMapping(value = "/getLatestApk", method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetLatestApk(HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlApkVersion apkVersion = new model.mdlApkVersion();
		String APIName = mdlResult.Title = "GetLatestApk";
		Gson gson = new Gson();
		String jsonIn = "";

		try{
			apkVersion = ApkVersionAdapter.GetVersion();

			if(!(apkVersion.versionCode == null || apkVersion.versionCode.equals("")))
			{
				mdlResult.StatusCode = "00";
				mdlResult.StatusMessage = "Berhasil";
				mdlResult.Value = apkVersion;
				String jsonOut = gson.toJson(mdlResult);
				logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
			else
			{
				mdlResult.StatusCode ="01";
				mdlResult.StatusMessage = "Apk version tidak ditemukan";
				String jsonOut = gson.toJson(mdlResult);
				logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="02";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "GetLatestApk", jsonIn, "");
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}
	
	@RequestMapping(value = "/getCms/{vodtID}",method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetCms(@PathVariable("vodtID") String vendorOrderDetailID, 
														@RequestHeader("Authorization") String Authorization,
														@RequestHeader("DriverID") String DriverID,
														HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "GetCmsData";
		Gson gson = new Gson();
		String jsonIn = vendorOrderDetailID;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (vendorOrderDetailID == null || vendorOrderDetailID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Nomor order kosong";
			return mdlResult;
		}
		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				modelKojaGetCms.mdlGetCms cmsData = new modelKojaGetCms.mdlGetCms();
				cmsData = KojaAdapter.LoadKojaCmsData(vendorOrderDetailID, DriverID);

				if(cmsData.cmsLink != null || !cmsData.cmsLink.equals(""))
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = cmsData;
					String jsonOut = gson.toJson(mdlResult);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="02";
					mdlResult.StatusMessage = "Tidak ada data cms";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}
	
	/*@RequestMapping(value = "/getEir/{vodtID}",method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetEir(@PathVariable("vodtID") String vendorOrderDetailID, 
														@RequestHeader("Authorization") String Authorization,
														@RequestHeader("DriverID") String DriverID,
														HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "GetEirData";
		Gson gson = new Gson();
		String jsonIn = vendorOrderDetailID;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (vendorOrderDetailID == null || vendorOrderDetailID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Nomor order kosong";
			return mdlResult;
		}
		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				modelKojaGetEir.mdlGetEir eirData = new modelKojaGetEir.mdlGetEir();
				eirData = KojaAdapter.LoadKojaEirData(vendorOrderDetailID, DriverID);

				if(eirData.eirLink != null || !eirData.eirLink.equals(""))
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = eirData;
					String jsonOut = gson.toJson(mdlResult);
					logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="02";
					mdlResult.StatusMessage = "Tidak ada data eir";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}*/
	
	
	// bikin service agar mobile app dapat barcode sebelum depo in
	// mobile sudah ada depoID saat berganti page dengan nama end point getAssignment
	
	// untuk bikin response api
	// StatusCode = 00 --> berhasil
	// StatusCode = 02 --> qr code tidak tersedia
	// StatusCode = 99 --> Session habis
	// StatusCode = 03 --> berhasil
	
	@RequestMapping(value = "/document/{type}/{typeID}",method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetDocument(@PathVariable("type") String typeDepoOrPort,
														@PathVariable("typeID") String typeID, 
														@RequestHeader("Authorization") String Authorization,
														@RequestHeader("DriverID") String DriverID,
														HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "GetDocument";
		Gson gson = new Gson();
		String jsonIn = DriverID;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (DriverID == null || DriverID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Driver ID tidak valid";
			return mdlResult;
		}
		
		if ((typeDepoOrPort == null || 	!typeDepoOrPort.isEmpty()) && typeDepoOrPort == null) {
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Parameter tidak valid";
			return mdlResult;
		}
		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				
				if (typeDepoOrPort.equalsIgnoreCase("depo")) {
					System.out.println(typeID);
					if (typeID.equals("DEPO-0003")) {
						System.out.println("cok");
						Context context = (Context)new InitialContext().lookup("java:comp/env");
						//get the base path for client(mobile) access from web.xml
				    	String smsContact = (String)context.lookup("param_sms_contact");
						List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
						listVendorOrderDetail = VendorOrderDetailAdapter.VendorOrderDetailLoadByDriverID(DriverID, smsContact);
						mdlLogBillingDepo billingDepo = LogBillingDepoAdapter.findLogBillingDepoByVendorOrderDetailID(listVendorOrderDetail.get(0).vendorOrderDetailID, DriverID);
//						mdlLogBillingDepo billingDepo = LogBillingDepoAdapter.findLogBillingDepoByOrderID("LG/201909/E0008", DriverID);
						
						if (billingDepo.getTransactionID() == null || "".equals(billingDepo.getTransactionID())) {
							mdlResult.StatusCode ="02";
							mdlResult.StatusMessage = "Tidak ada document";
							String jsonOut = gson.toJson(mdlResult);
							logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
						}else {
							mdlResult.StatusCode = "00";
							mdlResult.StatusMessage = "Berhasil";
							mdlResult.Value = billingDepo;
							
					    	String jsonOut = gson.toJson(mdlResult);
					    	logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
						}
						
					} else {
						System.out.println("coko");
						mdlResult.StatusCode = "00";
						mdlResult.StatusMessage = "Berhasil";
						
				    	String jsonOut = gson.toJson(mdlResult);
				    	logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
					}
					
				} else {
					System.out.println("coka");
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					
			    	String jsonOut = gson.toJson(mdlResult);
			    	logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				
				
				
		    	
		    	
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
		
	}
	
	@RequestMapping(value = "/pushCms", method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlResult pushCms(@RequestBody modelKojaGetCms.mdlGetCms param,
			HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "PushCms";
		Gson gson = new Gson();
		Boolean success = false;
		String jsonIn = gson.toJson(param);
		/*String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;*/

		if (param.containerNumber == null || param.containerNumber.equals("") ){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Nomor Container Kosong";
			return mdlResult;
		}else if (param.tidNumber == null || param.tidNumber.equals("") ){
			mdlResult.StatusCode = "02";
			mdlResult.StatusMessage = "Nomor Tid Kosong";
			return mdlResult;
		}else if (param.cmsLink == null || param.cmsLink.equals("") ){
			mdlResult.StatusCode = "03";
			mdlResult.StatusMessage = "Cms Tidak Ditemukan";
			return mdlResult;
		}else if (!param.userName.equals("koja-logol")){
			mdlResult.StatusCode = "04";
			mdlResult.StatusMessage = "Username Salah";
			return mdlResult;
		}
		else if (!param.password.equals("getCms9195194962")){
			mdlResult.StatusCode = "05";
			mdlResult.StatusMessage = "Password Salah";
			return mdlResult;
		}

		try{
			String vodtID = VendorOrderDetailAdapter.LoadVendorOrderDetailByContainerAndTid(param.containerNumber, param.tidNumber, param.userName).getVendorOrderDetailID();
			if (vodtID == null || vodtID.equals("")){
				mdlResult.StatusCode = "06";
				mdlResult.StatusMessage = "Nomor Penugasan Tidak Ditemukan";
				return mdlResult;
			}
			else{
				modelKojaGetCms.mdlGetCms cmsData = new modelKojaGetCms.mdlGetCms();
				//download url from link proccess
        		// Get the base naming context from web.xml
        		Context context = (Context)new InitialContext().lookup("java:comp/env");
        		
        		Date date = new Date();
        		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        		int year  = localDate.getYear();
        		String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
        		int day   = localDate.getDayOfMonth();
        		String separator = (String)context.lookup("param_separator");
        		String serverFileLocation = (String)context.lookup("param_file_location");
        		String serverIpport = (String)context.lookup("param_server_ipport");
        		String serverFileAccess = (String)context.lookup("param_file_access");
        		String destinationPath = serverFileLocation + "CMS" + separator + year + separator + month + separator + day + separator;
        		String finalPath = destinationPath + param.tidNumber + ".pdf";
        		
        		//initiate variable directory
          		File directory = null;
                directory = new File(destinationPath.substring(0, destinationPath.length() - 1));
                
                //check if directory is exist or not, if not, create it first
                if(!directory.exists() )
                    directory.mkdirs();
                
                InputStream inputStream = new URL(param.cmsLink).openStream();
            	Files.copy(inputStream, Paths.get(finalPath), StandardCopyOption.REPLACE_EXISTING);
            	//end of download url from link proccess
            	
            	//change link cms from koja to local file path 
        		cmsData.LINK = serverIpport + serverFileAccess + "CMS" + "/" + year + "/" + month + "/" + day + "/" + param.tidNumber + ".pdf";
        		
				cmsData.vendorOrderDetailID = vodtID;
				success = VendorOrderDetailAdapter.UpdateKojaCmsBooleanResult(cmsData);
				
				if(success)
				{
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = "Success";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				else
				{
					mdlResult.StatusCode ="07";
					mdlResult.StatusMessage = "Gagal update cms di sistem";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="08";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			LogAdapter.InsertLogExc(ex.toString(), "pushCms", jsonIn, param.userName);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
	}
	
	@RequestMapping(value = "/codeco",method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetAllCodeco(@RequestHeader("Authorization") String Authorization,
														@RequestHeader("DriverID") String DriverID,
														HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "GetCodecoByDoNumber";
		Gson gson = new Gson();
		String jsonIn = DriverID;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (DriverID == null || DriverID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Driver ID tidak valid";
			return mdlResult;
		}
		
		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				
				List<mdlCodeco> codecos = CodecoAdapter.LoadCodeco(DriverID);
				if (codecos.isEmpty() ||  codecos == null) {
					mdlResult.StatusCode ="02";
					mdlResult.StatusMessage = "Tidak ada data codeco";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}else {
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = codecos;
					
			    	String jsonOut = gson.toJson(mdlResult);
			    	logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
				
				
				
		    	
		    	
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
		
	}
	
	@RequestMapping(value = "/codeco/{doNumber}",method = RequestMethod.GET)
	public @ResponseBody model.mdlResult GetCodecoByDoNumber(@PathVariable("doNumber") String doNumber, 
														@RequestHeader("Authorization") String Authorization,
														@RequestHeader("DriverID") String DriverID,
														HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		String APIName = mdlResult.Title = "GetCodecoByDoNumber";
		Gson gson = new Gson();
		String jsonIn = DriverID;
		String token = Authorization.replace("Bearer ", "");
		Boolean checkTokenValidity = false;

		if (DriverID == null || DriverID.equals("")){
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Driver ID tidak valid";
			return mdlResult;
		}
		
		if (doNumber == null) {
			mdlResult.StatusCode = "01";
			mdlResult.StatusMessage = "Parameter tidak valid";
			return mdlResult;
		}
		try{
			checkTokenValidity = TokenAdapter.CheckTokenValidity(DriverID, token);
			if(checkTokenValidity){
				
				Context context = (Context)new InitialContext().lookup("java:comp/env");
				//get the base path for client(mobile) access from web.xml
		    	String smsContact = (String)context.lookup("param_sms_contact");
		    	
				List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
				listVendorOrderDetail = VendorOrderDetailAdapter.VendorOrderDetailLoadByDriverID(DriverID, smsContact);
				
				String vehicleNumber = listVendorOrderDetail.get(0).vehicleNumber;
				mdlCodeco codecos = CodecoAdapter.LoadCodecoByDoNumber(doNumber, vehicleNumber.replaceAll("\\s+", ""), DriverID);
				
				if (codecos == null) {
					mdlResult.StatusCode ="02";
					mdlResult.StatusMessage = "Tidak ada data codeco";
					String jsonOut = gson.toJson(mdlResult);
					logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}else {
					mdlResult.StatusCode = "00";
					mdlResult.StatusMessage = "Berhasil";
					mdlResult.Value = codecos;
					
			    	String jsonOut = gson.toJson(mdlResult);
			    	logger.info("SUCCESS. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
				}
		    	
		    	
			}
			else{
				mdlResult.StatusCode = "99";
				mdlResult.StatusMessage = "Session habis";
			}
		}catch(Exception ex){
			mdlResult.StatusCode ="03";
			mdlResult.StatusMessage = "Gagal memanggil service";
			String jsonOut = gson.toJson(mdlResult);
			logger.error("FAILED. API : " + APIName + ", method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut, ex);
		}
		return mdlResult;
		
	}
	
}
