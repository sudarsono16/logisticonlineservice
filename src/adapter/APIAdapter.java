package adapter;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.json.JSONObject;
import org.json.XML;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import model.mdlUpdateVendorOrderDetailStatus;
import modelKojaGetCms.mdlGetCms;
import modelKojaGetEir.mdlGetEir;

public class APIAdapter {

	public static Boolean KojaGetCms(mdlUpdateVendorOrderDetailStatus param, String driverID){
		String entityResponse = "";
	    String getServiceUrl = "";
	    Boolean validateCheckIn = false;
        
        String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.kojaUrl;
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<tpk:customService>"+
			                  "<username>"+param.kojaUsername+"</username>"+
			                  "<password>"+param.kojaPassword+"</password>"+
			                  "<fstream>{\"METHOD\":\"getCMS\",\"TID\":\""+param.tidNumber+"\",\"CNTR_ID\":\""+param.containerNumber+"\"}</fstream>"+
			               "</tpk:customService>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetCms.Envelope envelope = new modelKojaGetCms.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetCms.Envelope.class);
            
            mdlGetCms cmsData = new mdlGetCms();
            cmsData =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2CustomServiceResponse().getReturn(), mdlGetCms.class);
            
            if(cmsData.LINK == null || !cmsData.STATUS.equals("TRUE") || cmsData.LINK.equals("")){ //if invalid do this
            	LogAdapter.InsertLogExc(requestBody, "KojaGetCms", getServiceUrl , driverID);
            }
            else{ //if valid do this
            	//download url from link proccess
        		// Get the base naming context from web.xml
        		Context context = (Context)new InitialContext().lookup("java:comp/env");
        		
        		Date date = new Date();
        		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        		int year  = localDate.getYear();
        		String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
        		int day   = localDate.getDayOfMonth();
        		String separator = (String)context.lookup("param_separator");
        		String serverFileLocation = (String)context.lookup("param_file_location");
        		String serverIpport = (String)context.lookup("param_server_ipport");
        		String serverFileAccess = (String)context.lookup("param_file_access");
        		String destinationPath = serverFileLocation + "CMS" + separator + year + separator + month + separator + day + separator;
        		String finalPath = destinationPath + param.tidNumber + ".pdf";
        		
        		//initiate variable directory
          		File directory = null;
                directory = new File(destinationPath.substring(0, destinationPath.length() - 1));
                
                //check if directory is exist or not, if not, create it first
                if(!directory.exists() )
                    directory.mkdirs();
                
                InputStream inputStream = new URL(cmsData.LINK).openStream();
            	Files.copy(inputStream, Paths.get(finalPath), StandardCopyOption.REPLACE_EXISTING);
            	//end of download url from link proccess
            	
            	//change link cms from koja to local file path 
        		cmsData.LINK = serverIpport + serverFileAccess + "CMS" + "/" + year + "/" + month + "/" + day + "/" + param.tidNumber + ".pdf";
            	
            	cmsData.vendorOrderDetailID = param.vendorOrderDetailID;
            	VendorOrderDetailAdapter.UpdateKojaCms(cmsData);
            	validateCheckIn = true;
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetCms", getServiceUrl , driverID);
          }
        
        return validateCheckIn;
     }
	
	public static Boolean KojaGetEir(mdlUpdateVendorOrderDetailStatus param, String driverID){
		String entityResponse = "";
	    String getServiceUrl = "";
	    Boolean validateCheckIn = false;
        
        String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.kojaUrl;
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<tpk:customService>"+
			                  "<username>"+param.kojaUsername+"</username>"+
			                  "<password>"+param.kojaPassword+"</password>"+
			                  "<fstream>{\"METHOD\":\"getEIR\",\"TID\":\""+param.tidNumber+"\"}</fstream>"+
			               "</tpk:customService>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetEir.Envelope envelope = new modelKojaGetEir.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetEir.Envelope.class);
            
            mdlGetEir eirData = new mdlGetEir();
            eirData =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2CustomServiceResponse().getReturn(), mdlGetEir.class);
            
            if(eirData.LINK == null || !eirData.STATUS.equals("TRUE") || eirData.LINK.equals("")){ //if invalid do this
            	LogAdapter.InsertLogExc(requestBody, "KojaGetEir", getServiceUrl , driverID);
            }
            else{ //if valid do this
            	eirData.vendorOrderDetailID = param.vendorOrderDetailID;
            	VendorOrderDetailAdapter.UpdateKojaEir(eirData);
            	validateCheckIn = true;
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetEir", getServiceUrl , driverID);
          }
        
        return validateCheckIn;
     }
	
}
