package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlQueryExecute;

public class DriverAdapter {
	public static model.mdlDriverLogin GetDriverLoginData(model.mdlLogin mdlLogin){
		model.mdlDriverLogin mdlDriverLogin = new model.mdlDriverLogin();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call ws_GetDriverLoginData(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLogin.deviceID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLogin.token));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_GetDriverLoginData");
			while(jrs.next()){
				mdlDriverLogin.driverID = jrs.getString("DriverID");
				mdlDriverLogin.driverName = jrs.getString(2);
				mdlDriverLogin.password = jrs.getString("Password");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_GetDriverLoginData", sql , mdlLogin.deviceID);
		}
		return mdlDriverLogin;
	}
	
	public static model.mdlDriverLogin CheckLogin(model.mdlLogin mdlLogin){
		model.mdlDriverLogin mdlDriverLogin = new model.mdlDriverLogin();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call ws_CheckDriverLogin(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLogin.username));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLogin.password));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_CheckDriverLogin");
			while(jrs.next()){
				mdlDriverLogin.driverID = jrs.getString("DriverID");
				mdlDriverLogin.driverName = jrs.getString("DriverName");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CheckDriverLogin", sql , mdlLogin.username);
		}
		return mdlDriverLogin;
	}

	public static Boolean UpdateUserAndroidKey(model.mdlUserAndroidKey mdlAndroidKey){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		try{
			sql = "{call ws_UpdateUserAndroidKey(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlAndroidKey.driverID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlAndroidKey.androidKey));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_UpdateUserAndroidKey");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_UpdateUserAndroidKey", sql , mdlAndroidKey.driverID);
			//Globals.gReturn_Status = "Database Error";
		}

		return success;
	}
}
