package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class ApkVersionAdapter {

	public static model.mdlApkVersion GetVersion(){
		model.mdlApkVersion apkVersion = new model.mdlApkVersion();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call ws_LoadApkVersion()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "ws_LoadApkVersion");
			while(jrs.next()){
				apkVersion.versionCode = jrs.getInt("VersionCode");
				apkVersion.versionName = jrs.getString("VersionName");
				apkVersion.apkName = jrs.getString("ApkName");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_LoadApkVersion", sql , "");
		}
		return apkVersion;
	}
	
}
