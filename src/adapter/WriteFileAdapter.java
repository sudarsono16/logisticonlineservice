package adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

import com.logol.controller.controller;

import model.Globals;


public class WriteFileAdapter {
	final static Logger logger = Logger.getLogger(controller.class);

    public static void Write(InputStream fileinputstream, String paramFilePath, String fileoutputstream) {

        InputStream inputStream = null;
        OutputStream outputStream = null;
        
        File directory = null;
        String writeResult = "";
        //Globals.gReturn_Status = "";

        try {
            // read this file into InputStream
            //inputStream = new FileInputStream(fileinputstream);
            inputStream = fileinputstream;
            
            //initiate variable directory
            directory = new File(paramFilePath.substring(0, paramFilePath.length() - 1));
            
            //check if directory is exist or not, if not, create it first
            if(!directory.exists() )
                directory.mkdirs();
            
            //write the inputStream to a FileOutputStream
            outputStream = new FileOutputStream(new File(fileoutputstream));
            
            //write process
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            System.out.println("Done!");
            logger.info("SUCCESS. WriteImage");

        } catch (IOException e) {
            e.printStackTrace();
            writeResult = "Error Write File";
            logger.info("ERROR."+e.toString() );
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.info("ERROR."+e.toString());
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.info("ERROR."+e.toString());
                }

            }
        }
    }
    
    public static void Delete(String filestream)
    {
    	String deleteResult = "";
    	//Globals.gReturn_Status = "";
        try{

            File file = new File(filestream);

            if(file.delete()){
                System.out.println(file.getName() + " is deleted!");
            }else{
                System.out.println("Delete operation is failed.");
            }

        }catch(Exception e){

            e.printStackTrace();
            deleteResult = "Error Delete File";
            //Globals.gReturn_Status = "Error Delete File";
        }

    }
    
}
