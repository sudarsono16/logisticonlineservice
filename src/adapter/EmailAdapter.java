package adapter;

import java.util.List;
import java.util.Properties;
import java.util.ArrayList;
import javax.mail.*;
import javax.mail.internet.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlVendorOrderDetail;

import javax.activation.*;

public class EmailAdapter {

	public static void SendEmailDeliveredToCustomer(String vendorOrderDetailID)
	{
	   String command = "";	
	   try {
		   model.mdlCustomerEmail mdlCustomerEmail = GetCustomerEmailData(vendorOrderDetailID);
		   System.out.println(mdlCustomerEmail.orderManagementDetailID);
		   
		   String lOrderDetailID = mdlCustomerEmail.orderManagementDetailID;
	       List<mdlVendorOrderDetail> vendorOrderDetailList = new ArrayList<model.mdlVendorOrderDetail>();
	       vendorOrderDetailList.addAll(VendorOrderDetailAdapter.LoadVendorOrderDetailByCustomerOrder(lOrderDetailID));
		// Get the base naming context from web.xml
		  Context web_context = (Context)new InitialContext().lookup("java:comp/env");
		// Recipient's email ID needs to be mentioned.
	      String to = mdlCustomerEmail.email;
	      // Sender's email ID needs to be mentioned
	      String from  = (String)web_context.lookup("owner_email");
	      command = "Sending from "+ from +"to "+to;
	      // email host
	      String host = (String)web_context.lookup("host");
	      // Get system properties
	      String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	      Properties properties = System.getProperties();

	      // Setup mail server
	      properties.setProperty("mail.smtp.host", host);
	      properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
	      properties.setProperty("mail.smtp.socketFactory.fallback", "false");
	      properties.setProperty("mail.smtp.port", "465");
	      properties.setProperty("mail.smtp.socketFactory.port", "465");
	      properties.put("mail.smtp.auth", "true");
	      properties.put("mail.debug", "true");
	      properties.put("mail.store.protocol", "pop3");
	      properties.put("mail.transport.protocol", "smtp");
	      String mailUsername = from;
	      String mailPassword = (String)web_context.lookup("owner_email_password");

	      // Get the default Session object.
	      Session session = Session.getDefaultInstance(properties,
                  new Authenticator(){
              protected PasswordAuthentication getPasswordAuthentication() {
                 return new PasswordAuthentication(mailUsername, mailPassword);
              }});
	         // Create a default MimeMessage object.
	         MimeMessage message = new MimeMessage(session);
	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));
	         // Set To: header field of the header.
	         message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	         // Set Subject: header field
	         message.setSubject("LOGOL has delivered your shipment SI - "+mdlCustomerEmail.shippingInstruction+" !");
	         // Send the actual HTML message, as big as you like
	         
//	         String content = "<h3>Your order has been delivered with detail :<br> "
// 		 			+ "&nbsp;Shipping Instruction : " + mdlCustomerEmail.shippingInstruction +"<br>"
//  					+ "&nbsp;Do Shipping Line : " + mdlCustomerEmail.doShippingLine +"<br>"
//  					+ "&nbsp;Type : " + mdlCustomerEmail.orderType +"<br>"
//  					+ "&nbsp;Item Description : " + mdlCustomerEmail.itemDescription +"<br>"
//  					+ "&nbsp;Tier : " + mdlCustomerEmail.tier +"<br>"
//  					+ "&nbsp;Depo : " + mdlCustomerEmail.depo +"<br>"
//  					+ "&nbsp;Shipping : " + mdlCustomerEmail.shipping +"<br>"
//  					+ "&nbsp;Port : " + mdlCustomerEmail.port +"<br>"
//  					+ "&nbsp;Total Delivery : " + mdlCustomerEmail.totalQuantity +"<br>"
//  					+ "&nbsp;Status : Delivered<br><br>"
//  					+ "Please do not reply to this email. If you need further enquiry with your order please "
//			 			+ "contact enquiry@logol.co.id</h3>";
	         
	         String table = "";
	         int i = 1;
	         for (mdlVendorOrderDetail item : vendorOrderDetailList) {
				table += "<tr style='padding-left:10px; text-align:center;'>"+
	                    "<td  style='line-height:30px;'>"+ i +"</td>"+
	                    "<td style='line-height:30px;'>" +item.vehicleNumber+ "</td>"+
	                    "<td style='line-height:30px;'>" +item.driverName+ "</td>"+
	                    "<td style='line-height:30px;'>" +item.containerNumber+ "</td>"+
	                    "<td style='line-height:30px;'>" +item.sealNumber+ "</td>"+
	                    "<td style='line-height:30px;'>" +item.portIn+ "</td>"+
	                "</tr>";
	             i++;
	         }
	         
	         String content = "<body style='margin-left:20px;'>"+
	        	        "Your order has been delivered with detail :<br><br>"+
	        	        "<b>"+
	        	        "<table style='max-width: 1100px;' width='100%' cellpadding='0' cellspacing='0' border='0' class='wrapper' bgcolor='#FFFFFF'>"+
	        	            "<tr>"+
	        	                "<td style='width: 20%; vertical-align: top;text-align: left;'>Shipping Instruction</td>"+
	        	                "<td style='width: 2%; vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='width: 30%; vertical-align: top;text-align: left;'>"+ mdlCustomerEmail.shippingInstruction +"</td>"+

	        	                "<td style='width: 20%; vertical-align: top;text-align: left;'>Do Shipping Line</td>"+
	        	                "<td style='width: 2%; vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='width: 30%; vertical-align: top;text-align: left;'>" + mdlCustomerEmail.doShippingLine +"</td>"+
	        	            "</tr>"+
	        	            "<tr>"+
	        	                "<td style='vertical-align: top;text-align: left;'>Order ID</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>" +mdlCustomerEmail.orderManagementID+ "</td>"+

								"<td style='vertical-align: top;text-align: left;'>Item Description</td>"+
								"<td style='vertical-align: top;text-align: left;'>:</td>"+
								"<td style='vertical-align: top;text-align: left;'>" + mdlCustomerEmail.itemDescription +"</td>"+
	        	            "</tr>"+
	        	            "<tr>"+
		        	            "<td style='vertical-align: top;text-align: left;'>Factory</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>"+mdlCustomerEmail.factoryName+"</td>"+

	        	                "<td style='vertical-align: top;text-align: left;'>Container Quantity</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>" +mdlCustomerEmail.totalQuantity+" x " +mdlCustomerEmail.partyDescription+ "</td>"+
	        	            "</tr>"+
	        	            "<tr>"+
	        	                "<td style='vertical-align: top;text-align: left;'>Route</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>" + mdlCustomerEmail.route +"</td>"+

	        	                "<td style='vertical-align: top;text-align: left;'>Depo</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>" + mdlCustomerEmail.depo +"</td>"+
	        	            "</tr>"+
	        	            "<tr>"+
	        	                "<td style='vertical-align: top;text-align: left;'>Shipping</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>:</td>"+
	        	                "<td style='vertical-align: top;text-align: left;'>" + mdlCustomerEmail.shipping +"</td>"+

								"<td style='vertical-align: top;text-align: left;'>Status</td>"+
								"<td style='vertical-align: top;text-align: left;'>:</td>"+
								"<td style='vertical-align: top;text-align: left;'>Delivered</td>"+
	        	            "</tr>"+
	        	            "<tr>"+
								"<td style='vertical-align: top;text-align: left;'>Type</td>"+
								"<td style='vertical-align: top;text-align: left;'>:</td>"+
								"<td style='vertical-align: top;text-align: left;'>" + mdlCustomerEmail.orderType +"</td>"+

								
	        	            "</tr>"+
	        	        "</table>"+
	        	        "</b>"+
	        	        "<br>"+
	        	        "<table style='max-width: 1000px;' width='100%' cellpadding='0' cellspacing='0' border='1' class='wrapper' bgcolor='#FFFFFF'>"+
	        	            "<thead style='text-align:center; background-color: #d2d6de;'>"+
	        	                "<tr style='font-weight: bold; padding-left:20px;'>"+
	        	                    "<td style='width: 5%; line-height:30px;'>No</td>"+
	        	                    "<td style='width: 20%;'>Truck Info</td>"+
	        	                    "<td style='width: 20%;'>Driver</td>"+
	        	                    "<td style='width: 10%;'>No. Cont</td>"+
	        	                    "<td style='width: 10%;'>No. Seal</td>"+
	        	                    "<td style='width: 20%;'>Gate in Port</td>"+
	        	                "</tr>"+
	        	            "</thead>"+
	        	            "<tbody>"+
	        	            		table+
	        	            "</tbody>"+
	        	        "</table>"+
	        	        "<br>"+
	        	        "Please do not reply to this email. If you need further inquiry with your order please contact inquiry@logol.co.id"+
	        	"</body>";
	         message.setContent(content, "text/html");
	         // Send message
	         Transport.send(message);
	         System.out.println("Sent message successfully....");
	      } catch (Exception mex) {
	         mex.printStackTrace();
	         LogAdapter.InsertLogExc(mex.toString(), "SendEmailCustomer", command , vendorOrderDetailID);
	      }
	}

	public static model.mdlCustomerEmail GetCustomerEmailData(String vendorOrderDetailID){
		model.mdlCustomerEmail mdlCustomerEmail = new model.mdlCustomerEmail();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			sql = "{call ws_GetCustomerEmailData(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderDetailID ));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_GetCustomerEmailData");
			while(rowset.next())
			{
				mdlCustomerEmail.setShippingInstruction(rowset.getString("ShippingInvoiceID"));
				mdlCustomerEmail.doShippingLine = rowset.getString("DeliveryOrderID");
				mdlCustomerEmail.orderType = rowset.getString("OrderType");
				mdlCustomerEmail.itemDescription = rowset.getString("ItemDescription");
				mdlCustomerEmail.totalQuantity = rowset.getInt("TotalQuantity");
				mdlCustomerEmail.totalPrice = rowset.getDouble("TotalPrice");
				mdlCustomerEmail.email = rowset.getString("Email");
				mdlCustomerEmail.orderManagementID = rowset.getString("OrderManagementID");
				mdlCustomerEmail.orderManagementDetailID = rowset.getString("OrderManagementDetailID");
				mdlCustomerEmail.setFactoryName(rowset.getString(21));
				mdlCustomerEmail.tier = rowset.getString(16);
				mdlCustomerEmail.shipping = rowset.getString(19);
				mdlCustomerEmail.depo = rowset.getString(20);
				mdlCustomerEmail.partyDescription = rowset.getString(25);
				
				
				mdlCustomerEmail.route = rowset.getString(22) + " - " + rowset.getString("PortName") + "(" + rowset.getString("PortUTC")+ ")";
				
				
				
				
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_GetCustomerEmailData", sql, vendorOrderDetailID);
		}
		return mdlCustomerEmail;
	}

	public static void SendEmailCancelledToVendor(String vendorOrderDetailID)
	{
	   String command = "";
	   try {
		   model.mdlVendorEmail mdlVendorEmail = GetVendorEmailData(vendorOrderDetailID);
		// Get the base naming context from web.xml
		  Context web_context = (Context)new InitialContext().lookup("java:comp/env");
		// Recipient's email ID needs to be mentioned.
	      String to = mdlVendorEmail.email;
	      // Sender's email ID needs to be mentioned
	      String from  = (String)web_context.lookup("owner_email");
	      command = "Sending from "+ from +"to "+to;
	      // email host
	      String host = (String)web_context.lookup("host");
	      // Get system properties
	      String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	      Properties properties = System.getProperties();

	      // Setup mail server
	      properties.setProperty("mail.smtp.host", host);
	      properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
	      properties.setProperty("mail.smtp.socketFactory.fallback", "false");
	      properties.setProperty("mail.smtp.port", "465");
	      properties.setProperty("mail.smtp.socketFactory.port", "465");
	      properties.put("mail.smtp.auth", "true");
	      properties.put("mail.debug", "true");
	      properties.put("mail.store.protocol", "pop3");
	      properties.put("mail.transport.protocol", "smtp");
	      String mailUsername = from;
	      String mailPassword = (String)web_context.lookup("owner_email_password");

	      // Get the default Session object.
	      Session session = Session.getDefaultInstance(properties,
                  new Authenticator(){
              protected PasswordAuthentication getPasswordAuthentication() {
                 return new PasswordAuthentication(mailUsername, mailPassword);
              }});
	         // Create a default MimeMessage object.
	         MimeMessage message = new MimeMessage(session);
	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));
	         // Set To: header field of the header.
	         message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	         // Set Subject: header field
	         message.setSubject("LOGOL - Your Driver Has Cancelled your order!");
	         // Send the actual HTML message, as big as you like
	         message.setContent("<h3>Your order has been cancelled with detail :<br> "
	        		 			+ "&nbsp;Order ID : " + mdlVendorEmail.vendorOrderID +"<br>"
	        		 			+ "&nbsp;Order Detail ID : " + mdlVendorEmail.vendorOrderDetailID +"<br>"
	         					+ "&nbsp;Order Type : " + mdlVendorEmail.orderType +"<br>"
	         					+ "&nbsp;Item Description : " + mdlVendorEmail.itemDescription +"<br>"
	         					+ "&nbsp;Assigned Driver ID : " + mdlVendorEmail.driverID +"<br>"
	         					+ "&nbsp;Assigned Driver Name : " + mdlVendorEmail.driverName +"<br>"
	         					+ "&nbsp;Status : Cancelled<br>"
	         					+ "&nbsp;Cancelled Reason : " + mdlVendorEmail.cancelledReason + "<br><br>"
	         					+ "Please do not reply to this email. If you need further enquiry with your order please "
        			 			+ "contact enquiry@logol.co.id</h3>", "text/html");
	         // Send message
	         Transport.send(message);
	         System.out.println("Sent message successfully....");
	      } catch (Exception mex) {
	         mex.printStackTrace();
	         LogAdapter.InsertLogExc(mex.toString(), "SendEmailVendor", command , vendorOrderDetailID);
	      }
	}

	public static model.mdlVendorEmail GetVendorEmailData(String vendorOrderDetailID){
		model.mdlVendorEmail mdlVendorEmail = new model.mdlVendorEmail();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			sql = "{call ws_GetVendorEmailData(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderDetailID ));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_GetVendorEmailData");
			while(rowset.next())
			{
				mdlVendorEmail.vendorOrderID = rowset.getString("VendorOrderID");
				mdlVendorEmail.vendorOrderDetailID = rowset.getString("VendorOrderDetailID");
				mdlVendorEmail.driverID = rowset.getString("DriverID");
				mdlVendorEmail.driverName = rowset.getString(4);
				mdlVendorEmail.vehicleNumber = rowset.getString("VehicleNumber");
				mdlVendorEmail.orderType = rowset.getString("OrderType");
				mdlVendorEmail.itemDescription = rowset.getString("ItemDescription");
				mdlVendorEmail.customerID = rowset.getString("CustomerID");
				mdlVendorEmail.customerName = rowset.getString(9);
				mdlVendorEmail.email = rowset.getString("Email");
				mdlVendorEmail.cancelledReason = rowset.getString("CancelReason");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_GetVendorEmailData", sql, vendorOrderDetailID);
		}
		return mdlVendorEmail;
	}
	
}
