package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.mdlCodeco;

public class CodecoAdapter {
	
	final static Logger logger = Logger.getLogger(CodecoAdapter.class);
	
	public static List<mdlCodeco> LoadCodeco(String user) {
		List<mdlCodeco> listCodeco = new ArrayList<mdlCodeco>();
		CachedRowSet jrs = null;
		//Globals.gcommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadCodeco()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "sp_LoadCodeco");

			while(jrs.next()){
				mdlCodeco codeco = new mdlCodeco();
				codeco.setCodecoID(jrs.getString("CodecoID"));
				codeco.setContainerNumber(jrs.getString("ContainerNumber") != null ? jrs.getString("ContainerNumber") : "");
				codeco.setIsoCode(jrs.getString("IsoCode") != null ? jrs.getString("IsoCode") : "");
				codeco.setDoNumber(jrs.getString("DoNumber") != null ? jrs.getString("DoNumber") : "");
				codeco.setDepoOutDateTime(jrs.getString("DepoOutDateTime") != null ? jrs.getString("DepoOutDateTime") : "");
				codeco.setSealNumber(jrs.getString("SealNumber") != null ? jrs.getString("SealNumber") : "");
				codeco.setPlatNumber(jrs.getString("PlatNumber") != null ? jrs.getString("PlatNumber") : "");
				listCodeco.add(codeco);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "sp_LoadCodeco", sql , user);
		}

		return listCodeco;
	}
	
	public static String InsertCodeco(mdlCodeco codeco, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlCodeco CheckDuplicateCodeco = LoadCodecoByID(codeco.getCodecoID(), user);
			if(CheckDuplicateCodeco.getCodecoID()== null){
				String newCodecoID = CreateCodeoID(user);
				sql = "{call sp_InsertCodeco(?,?,?,?,?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newCodecoID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", codeco.getContainerNumber()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", codeco.getIsoCode()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", codeco.getDoNumber()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", codeco.getDepoOutDateTime()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", codeco.getSealNumber()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", codeco.getPlatNumber()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertCodeco");

				result = success == true ? "Success Insert Codeco" : "Database Error";
			}
			//if duplicate
			else{
				result = "Port sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCodeco", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static mdlCodeco LoadCodecoByID (String codecoID, String user) {
		mdlCodeco codeco = new mdlCodeco();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gcommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadCodecoByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", codecoID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_LoadCodecoByID");

			while(jrs.next()){
				codeco.setCodecoID(jrs.getString("CodecoID"));
				codeco.setContainerNumber(jrs.getString("ContainerNumber"));
				codeco.setIsoCode(jrs.getString("IsoCode"));
				codeco.setDoNumber(jrs.getString("DoNumber"));
				codeco.setDepoOutDateTime(jrs.getString("DepoOutDateTime"));
				codeco.setSealNumber(jrs.getString("SealNumber"));
				codeco.setPlatNumber(jrs.getString("PlatNumber"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "sp_LoadCodecoByID", sql , user);
		}

		return codeco;
	}
	
	public static mdlCodeco LoadCodecoByDoNumber (String doNumber, String vehicleNumber, String user) {
		mdlCodeco codeco = new mdlCodeco();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gcommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadCodecoByDoNumber(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", doNumber) );
			listParam.add(QueryExecuteAdapter.QueryParam("string", vehicleNumber) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_LoadCodecoByDoNumber");

			while(jrs.next()){
				
				codeco.setCodecoID(jrs.getString("CodecoID"));
				codeco.setContainerNumber(jrs.getString("ContainerNumber"));
				codeco.setIsoCode(jrs.getString("IsoCode"));
				codeco.setDoNumber(jrs.getString("DoNumber"));
				codeco.setDepoOutDateTime(jrs.getString("DepoOutDateTime"));
				codeco.setSealNumber(jrs.getString("SealNumber"));
				codeco.setPlatNumber(jrs.getString("PlatNumber"));
				
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "sp_LoadCodecoByDoNumber", sql , user);
		}

		return codeco;
	}
	
	public static String CreateCodeoID(String user){
		String lastCodecoID = GetLastCodecoID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		//String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		//String stringInc = "000001";
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		System.out.println(datetime);
		String stringInc = "00001";

		if ((lastCodecoID != null) && (!lastCodecoID.equals("")) ){
			String[] partsCodecoID = lastCodecoID.split("-");
			String partDate = partsCodecoID[1];
			String partNumber = partsCodecoID[2];
			System.out.println(partDate);
			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
				//stringInc = String.format("%06d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		//sb.append("ORHD-").append(datetime).append("-").append(stringInc);
		sb.append("CODECO-").append(datetime).append("-").append(stringInc);
		String OrderManagementID = sb.toString();
		return OrderManagementID;
	}
	
	public static String GetLastCodecoID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String CodecoID = "";

		try {
			sql = "{call sp_GetLastCodecoID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "sp_GetLastCodecoID");

			while(jrs.next()){
				CodecoID = jrs.getString("CodecoID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "sp_GetLastCodecoID", sql , user);
		}

		return CodecoID;
	}

}
