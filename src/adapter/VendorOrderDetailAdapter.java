package adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlDebitNoteDetail;
import model.mdlQueryExecute;
import model.mdlSettingInvoice;

public class VendorOrderDetailAdapter {
	public static List<model.mdlVendorOrderDetail> VendorOrderDetailLoadByDriverID(String driverID, String contact) {
		List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		//String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		
		//Initiate calendar library
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		
		//get h-5 date
        c.setTime(currentDate);
        c.add(Calendar.DATE, -5);
        Date currentDateMinusFive = c.getTime();
        
		//get h date
        c.setTime(currentDate);
        /*c.add(Calendar.DATE, 5);*/
        /*Date currentDatePlusFive = c.getTime();*/
        Date currentDateToday = c.getTime();
        
		try{
			sql = "{call ws_VendorOrderDetailLoadByDriverID(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", driverID));
			//listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateFormat.format(currentDateMinusFive).toString() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateFormat.format(currentDateToday).toString() ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_VendorOrderDetailLoadByDriverID");

			while(jrs.next()){
				model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
				mdlVendorOrderDetail.vendorOrderDetailID = jrs.getString("VendorOrderDetailID");
				mdlVendorOrderDetail.vendorOrderID = jrs.getString("VendorOrderID");
				mdlVendorOrderDetail.vehicleNumber = jrs.getString("VehicleNumber");
				mdlVendorOrderDetail.vendorDetailStatus = jrs.getString("VendorDetailStatus");
				mdlVendorOrderDetail.containerNumber = jrs.getString("ContainerNumber");
				mdlVendorOrderDetail.sealNumber = jrs.getString("SealNumber");
				mdlVendorOrderDetail.driverID = jrs.getString("DriverID");
				mdlVendorOrderDetail.driverName = jrs.getString("DriverName");
				mdlVendorOrderDetail.vendorID = jrs.getString("VendorID");
				mdlVendorOrderDetail.vendorName = jrs.getString(10);
				mdlVendorOrderDetail.stuffingDate = jrs.getString("StuffingDate");
				mdlVendorOrderDetail.tierID = jrs.getString("TierID");
				mdlVendorOrderDetail.tierDescription = jrs.getString(13);
				mdlVendorOrderDetail.containerTypeID = jrs.getString("ContainerTypeID");
				mdlVendorOrderDetail.containerTypeDescription = jrs.getString(15);
				mdlVendorOrderDetail.itemDescription = jrs.getString("ItemDescription");
				mdlVendorOrderDetail.factoryID = jrs.getString("FactoryID");
				mdlVendorOrderDetail.factoryName = jrs.getString(18);
				mdlVendorOrderDetail.factoryPic = jrs.getString(19);
				mdlVendorOrderDetail.factoryAddress = jrs.getString(20);
				mdlVendorOrderDetail.factoryOfficePhone = jrs.getString(21);
				mdlVendorOrderDetail.factoryMobilePhone = jrs.getString(22);
				mdlVendorOrderDetail.portID = jrs.getString("PortID");
				mdlVendorOrderDetail.portName = jrs.getString(24);
				mdlVendorOrderDetail.portUTC = jrs.getString("PortUTC");
				mdlVendorOrderDetail.shippingID = jrs.getString("ShippingID");
				mdlVendorOrderDetail.shippingName = jrs.getString(27);
				mdlVendorOrderDetail.shippingAddress = jrs.getString(28);
				mdlVendorOrderDetail.depoID = jrs.getString("DepoID");
				mdlVendorOrderDetail.depoName = jrs.getString(30);
				mdlVendorOrderDetail.depoAddress = jrs.getString(31);
				mdlVendorOrderDetail.customerID = jrs.getString("CustomerID");
				mdlVendorOrderDetail.customerName = jrs.getString(33);
				mdlVendorOrderDetail.pic1 = jrs.getString("PIC1");
				mdlVendorOrderDetail.pic2 = jrs.getString("PIC2");
				mdlVendorOrderDetail.pic3 = jrs.getString("PIC3");
				mdlVendorOrderDetail.address = jrs.getString(37);
				mdlVendorOrderDetail.officePhone = jrs.getString(38);
				mdlVendorOrderDetail.mobilePhone = jrs.getString(39);
				mdlVendorOrderDetail.districtID = jrs.getString("DistrictID");
				mdlVendorOrderDetail.districtName = jrs.getString(41);
				mdlVendorOrderDetail.orderType = jrs.getString("OrderType");
				mdlVendorOrderDetail.peb = jrs.getString("PEB");
				mdlVendorOrderDetail.npe = jrs.getString("NPE");
				mdlVendorOrderDetail.shippingInstruction = jrs.getString("ShippingInvoiceID");
				mdlVendorOrderDetail.doShippingLine = jrs.getString("DeliveryOrderID");
				mdlVendorOrderDetail.yellowCard = jrs.getString("YellowCard");
				mdlVendorOrderDetail.party = jrs.getInt("Quantity");
				mdlVendorOrderDetail.orderManagementID = jrs.getString("OrderManagementID");
				
				mdlVendorOrderDetail.depoIn = ""; mdlVendorOrderDetail.depoOut = "";
				mdlVendorOrderDetail.factoryIn = ""; mdlVendorOrderDetail.factoryOut = "";
				mdlVendorOrderDetail.portIn = ""; mdlVendorOrderDetail.portOut = "";
				if(jrs.getString("DepoIn") != null)
					mdlVendorOrderDetail.depoIn = jrs.getString("DepoIn");
				
				if(jrs.getString("DepoOut") != null)
					mdlVendorOrderDetail.depoOut = jrs.getString("DepoOut");
				
				if(jrs.getString("FactoryIn") != null)
					mdlVendorOrderDetail.factoryIn = jrs.getString("FactoryIn");
				
				if(jrs.getString("FactoryOut") != null)
					mdlVendorOrderDetail.factoryOut = jrs.getString("FactoryOut");
				
				if(jrs.getString("PortIn") != null)
					mdlVendorOrderDetail.portIn = jrs.getString("PortIn");
				
				if(jrs.getString("PortOut") != null)
					mdlVendorOrderDetail.portOut = jrs.getString("PortOut");
				
				mdlVendorOrderDetail.isoCode = jrs.getString("Tps_Iso");
				mdlVendorOrderDetail.weight = jrs.getInt("Tps_Weight");
				mdlVendorOrderDetail.transactionType = jrs.getString("Tps_TransactionType");
				mdlVendorOrderDetail.vessel = jrs.getString("VesselName");
				mdlVendorOrderDetail.voyage = jrs.getString("VoyageNo");
				mdlVendorOrderDetail.etb = jrs.getString("Tps_ETB");
				mdlVendorOrderDetail.closing = jrs.getString("Tps_Closing");
				mdlVendorOrderDetail.pod1 = jrs.getString("Tps_Pod1");
				mdlVendorOrderDetail.pod2 = jrs.getString("Tps_Pod2");
				mdlVendorOrderDetail.companyName = jrs.getString(33);
				mdlVendorOrderDetail.imoCode = jrs.getString("Tps_Imo");
				mdlVendorOrderDetail.customDoc = jrs.getString("Tps_CustomDoc");
				mdlVendorOrderDetail.bat = jrs.getString("Tps_Bat");
				mdlVendorOrderDetail.invoiceNo = jrs.getString("Tps_InvoiceNo");
				mdlVendorOrderDetail.batDate = jrs.getString("Tps_BatDate");
				mdlVendorOrderDetail.contact = contact.split("/")[0];
				mdlVendorOrderDetail.tidNumber = jrs.getString("TID_Number");
				mdlVendorOrderDetail.contactName = contact.split("/")[1];
				mdlVendorOrderDetail.owner = jrs.getString("Tps_CurOwner");
				mdlVendorOrderDetail.shipperName = jrs.getString(72);
				
				listVendorOrderDetail.add(mdlVendorOrderDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_VendorOrderDetailLoadByDriverID", sql , driverID);
		}
		return listVendorOrderDetail;
	}

	public static Boolean UpdateVendorOrderDetailStatus(model.mdlUpdateVendorOrderDetailStatus mdlUpdateOrderStatus){
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction;
		String dateNow;
		if(mdlUpdateOrderStatus.datetime == null || mdlUpdateOrderStatus.datetime.equals(""))
			dateNow = LocalDateTime.now().toString();
		else
			dateNow = mdlUpdateOrderStatus.datetime;
		
		try{
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateOrderStatus.vendorOrderDetailID));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlUpdateOrderStatus.vendorDetailStatus));
			if (mdlUpdateOrderStatus.vendorDetailStatus == 1 ){ //On Process
				mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusKmStart(?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlUpdateOrderStatus.kmStart));
			}else if (mdlUpdateOrderStatus.vendorDetailStatus == 6 ){ //On Depo
				mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusTimestamp(?,?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "DepoIn"));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow ));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "" ));
			}else if (mdlUpdateOrderStatus.vendorDetailStatus == 7 ){ // Depo Checkout
				//mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusDepoOut(?,?,?,?,?,?)}";
				mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusDepoOut(?,?,?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateOrderStatus.containerNumber ));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateOrderStatus.sealNumber ));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateOrderStatus.signatureName));
			}else if (mdlUpdateOrderStatus.vendorDetailStatus == 5 ){ // On Factory
				mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusTimestamp(?,?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "FactoryIn"));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "" ));
			}else if (mdlUpdateOrderStatus.vendorDetailStatus == 2 ){ // On Delivery
				if (mdlUpdateOrderStatus.orderType.equalsIgnoreCase("EXPORT")){
					//mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusTimestamp(?,?,?,?,?)}";
					mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusTimestamp(?,?,?,?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "FactoryOut"));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateOrderStatus.signatureName));
				}else{
					mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusTimestamp(?,?,?,?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "PortOut"));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "" ));
				}
			}else if (mdlUpdateOrderStatus.vendorDetailStatus == 3 ){ // On Port
				mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusTimestamp(?,?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "PortIn"));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "" ));
			}else if (mdlUpdateOrderStatus.vendorDetailStatus == 4 ){ // Delivered
				if (mdlUpdateOrderStatus.orderType.equalsIgnoreCase("EXPORT")){
					mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusDelivered(?,?,?,?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "EXPORT"));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlUpdateOrderStatus.kmEnd));
					//mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlUpdateOrderStatus.kmEnd));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
				}else{
					mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusDelivered(?,?,?,?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "IMPORT"));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlUpdateOrderStatus.kmEnd));
					//mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlUpdateOrderStatus.kmEnd));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
				}
			}else if (mdlUpdateOrderStatus.vendorDetailStatus == 10 ){
				mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatusCancelReason(?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateOrderStatus.driverCancelReason ));
			}else{
				mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatus(?,?,?)}";
			}
			sql = mdlQueryTransaction.sql;
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateOrderStatus.updatedBy ));
			listmdlQueryTransaction.add(mdlQueryTransaction);

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "ws_UpdateVendorOrderDetailStatus");

			//if (success && mdlUpdateOrderStatus.vendorDetailStatus == 4){
			//EmailAdapter.SendEmailDeliveredToCustomer(mdlUpdateOrderStatus.vendorOrderDetailID);
			//}
			//else 
			if (success && mdlUpdateOrderStatus.vendorDetailStatus == 10)
				EmailAdapter.SendEmailCancelledToVendor(mdlUpdateOrderStatus.vendorOrderDetailID);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_UpdateVendorOrderDetailStatus", sql , mdlUpdateOrderStatus.vendorOrderDetailID);
			//Globals.gReturn_Status = "Database Error";
		}
		return success;
	}

	public static void CheckOrderStatus (String vendorOrderDetailID ){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		CachedRowSet rowset = null;
		try{
			sql = "{call sp_CheckUpdateStatus(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderDetailID));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_CheckUpdateStatus");
			
			//check if shipping instruction have been completed or not, if yes, send email to the customer
			listParam = new ArrayList<mdlQueryExecute>();
			sql = "{call sp_VendorOrderDetailLoadByDetailID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderDetailID ));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_VendorOrderDetailLoadByDetailID");
			while(rowset.next())
			{
				mdlSettingInvoice settingInvoice = DebitNoteAdapter.getSetingInvoiceByCusID(rowset.getString("OrderManagementID"), rowset.getString("CustomerID"));
				//insert credit note
				if(rowset.getInt("VendorStatus") == 3){
					model.mdlCreditNote creditNote = new model.mdlCreditNote();
					Double vendorRate = rowset.getDouble(18);
					creditNote.vendorID = rowset.getString("VendorID");
					creditNote.orderManagementID = rowset.getString("OrderManagementID");
					creditNote.vendorOrderID = rowset.getString("VendorOrderID");
					creditNote.date = LocalDateTime.now().toString();
					int totalTrucking = rowset.getInt("Quantity") * vendorRate.intValue();
					
					int pph23 = (totalTrucking * settingInvoice.getPph23())/100;
					
					creditNote.amount = totalTrucking - pph23;
					creditNote.status = "Unpaid";
					CreditNoteAdapter.InsertCreditNote(creditNote);
					
					//insert debit note
					if(rowset.getInt("OrderStatus") == 4){
						model.mdlDebitNote debitNote = new model.mdlDebitNote();
						/*Double customerRate = rowset.getDouble(16);*/
						debitNote.customerID = rowset.getString("CustomerID");
						debitNote.orderManagementID = rowset.getString("OrderManagementID");
						debitNote.date = LocalDateTime.now().toString();
						/*debitNote.amount = rowset.getInt("TotalQuantity") * customerRate.intValue();*/
						int totalCustomeClearance = settingInvoice.getCustomClearance() * rowset.getInt("TotalQuantity");
						int totalPPN = ((settingInvoice.getTruckingAmount() + totalCustomeClearance) * settingInvoice.getPpn())/100;
						int subTotal = settingInvoice.getTruckingAmount() + totalCustomeClearance + totalPPN;
						int total = subTotal - (((settingInvoice.getTruckingAmount() + totalCustomeClearance) * settingInvoice.getPph23())/100);
						debitNote.amount = 	total;
						debitNote.status = "Unpaid";
						debitNote.finalRate = settingInvoice.getFinalRate();
						debitNote.customClearance = settingInvoice.getCustomClearance();
						debitNote.ppn = settingInvoice.getPpn();
						debitNote.pph23 = settingInvoice.getPph23();
						
						DebitNoteAdapter.InsertDebitNote(debitNote);
						
						List<mdlDebitNoteDetail> dnDetail = DebitNoteAdapter.getSetingInvoiceByOrderManagementID(debitNote.orderManagementID, debitNote.debitNoteNumber, "mobile");
						for (mdlDebitNoteDetail dnDetail2 : dnDetail) {
							DebitNoteAdapter.insertDebitNoteDetail(dnDetail2, "mobile");
						}
						
						int amount = DebitNoteAdapter.amountInvoice(debitNote.debitNoteNumber, "mobile");
						DebitNoteAdapter.updateFinalRateByDebitNoteNumberDetailID(debitNote.debitNoteNumber, amount, "mobile");
						
						EmailAdapter.SendEmailDeliveredToCustomer(vendorOrderDetailID);
					}
				}
			}
		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CheckUpdateStatus", sql , vendorOrderDetailID);
			//Globals.gReturn_Status = "Database Error";
		}
	}

	public static Boolean UpdateDriverKmEnd(model.mdlUpdateDriverKmEnd mdlUpdateDriverKmEnd){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		try{
			sql = "{call ws_UpdateDriverKmEnd(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlUpdateDriverKmEnd.vendorOrderDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlUpdateDriverKmEnd.kmEnd));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_UpdateDriverKmEnd");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_UpdateDriverKmEnd", sql , mdlUpdateDriverKmEnd.vendorOrderDetailID);
			//Globals.gReturn_Status = "Database Error";
		}

		return success;
	}
	
	public static List<model.mdlVendorOrderDetail> LoadVendorOrderDetailByCustomerOrder(String CustomerOrderDetailID) {
		List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadVendorOrderDetailByCustomerOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", CustomerOrderDetailID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_LoadVendorOrderDetailByCustomerOrder");

			while(jrs.next()){
				model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
				mdlVendorOrderDetail.setDriverID(jrs.getString("DriverID"));
				mdlVendorOrderDetail.setDriverName(jrs.getString("DriverName"));
				mdlVendorOrderDetail.setVehicleNumber(jrs.getString("VehicleNumber"));
				mdlVendorOrderDetail.setVendorDetailStatus(jrs.getString("VendorDetailStatus"));
				mdlVendorOrderDetail.setContainerNumber(jrs.getString("ContainerNumber"));
				mdlVendorOrderDetail.setSealNumber(jrs.getString("SealNumber"));
				mdlVendorOrderDetail.setVendorID(jrs.getString("VendorID"));
				mdlVendorOrderDetail.setVendorName(jrs.getString("Name"));
				mdlVendorOrderDetail.setPortIn(ConvertDateTimeHelper.formatDate(jrs.getString("portIn"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				
				listVendorOrderDetail.add(mdlVendorOrderDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDetailByCustomerOrder", sql , CustomerOrderDetailID);
		}
		return listVendorOrderDetail;
	}
	
	public static String LoadIsoCodeByContainer(String ContainerNumber, String DriverID) {
		String isoCode = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call ws_IsoGetByContainerNumber(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", ContainerNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_IsoGetByContainerNumber");

			while(jrs.next()){
				isoCode = jrs.getString("IsoCode");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadIsoCodeByContainer", sql , DriverID);
		}
		return isoCode;
	}

	public static Boolean InsertUpdateIsoCode(model.mdlVendorOrderDetail param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		try{
			sql = "{call ws_IsoUpdate(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.containerNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.isoCode));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_IsoUpdate");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_IsoUpdate", sql , param.vendorOrderDetailID);
		}

		return success;
	}
	
	/*public static void UpdateKojaCms(modelKojaGetCms.mdlGetCms param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		try{
			sql = "{call ws_CmsDataUpdate(?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CMS_NO.get(0) ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.PRINT_CMS_DATETIME.get(0) ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CUR_OWNER.get(0) ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CNTR_STATUS.get(0) ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CNTR_SIZE.get(0) ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CNTR_TYPE.get(0) ));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", Integer.valueOf(param.GROSS_WEIGHT_DOCUMENTED.get(0)) ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.LS_LEVEL_1.get(0)+"-"+param.LS_LEVEL_2.get(0)+"-"+param.LS_LEVEL_3.get(0)+"-"+param.LS_LEVEL_4.get(0)));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_CmsDataUpdate");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CmsDataUpdate", sql , param.vendorOrderDetailID);
		}

		return;
	}*/
	
	public static void UpdateKojaCms(modelKojaGetCms.mdlGetCms param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		try{
			sql = "{call ws_CmsLinkUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.LINK ));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_CmsLinkUpdate");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CmsLinkUpdate", sql , param.vendorOrderDetailID);
		}

		return;
	}
	
	public static Boolean UpdateKojaCmsBooleanResult(modelKojaGetCms.mdlGetCms param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		try{
			sql = "{call ws_CmsLinkUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.LINK ));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_CmsLinkUpdate");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CmsLinkUpdate", sql , param.vendorOrderDetailID);
		}

		return success;
	}
	
	public static void UpdateKojaEir(modelKojaGetEir.mdlGetEir param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		try{
			sql = "{call ws_EirLinkUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.LINK ));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_EirLinkUpdate");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_EirLinkUpdate", sql , param.vendorOrderDetailID);
		}

		return;
	}
	
	public static model.mdlVendorOrderDetail LoadVendorOrderDetailByContainerAndTid(String ContainerNumber, String TidNumber, String userName) {
		model.mdlVendorOrderDetail vodtData = new model.mdlVendorOrderDetail();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_VendorOrderDetailLoadByContainerAndTid(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", ContainerNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", TidNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_VendorOrderDetailLoadByContainerAndTid");

			while(jrs.next()){
				vodtData.setVendorOrderDetailID(jrs.getString(1));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDetailByContainerAndTid", sql , userName);
		}
		return vodtData;
	}
	
}
