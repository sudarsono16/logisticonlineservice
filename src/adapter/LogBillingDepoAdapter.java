package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.apache.log4j.Logger;

import com.logol.controller.controller;

import database.QueryExecuteAdapter;
import model.mdlLogBillingDepo;

public class LogBillingDepoAdapter {
	
	final static Logger logger = Logger.getLogger(LogBillingDepoAdapter.class);
	
	public static mdlLogBillingDepo findLogBillingDepoByOrderID(String lOrderID, String user) {
		mdlLogBillingDepo logBillingDepo = new mdlLogBillingDepo();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_GetLogBillingDepoByOrderManagementID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lOrderID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "findLogBillingDepoByOrderID");

			while(jrs.next()){
				System.out.println(sql);
				logBillingDepo.setOrderManagementID(jrs.getString("OrderManagementID"));
				logBillingDepo.setBillingStatus(jrs.getInt("BillingStatus"));
				logBillingDepo.setNote(jrs.getString("Note") != null ? jrs.getString("Note") : "");
				logBillingDepo.setUrlProformaInvoice(jrs.getString("UrlProformaInvoice") != null ? jrs.getString("UrlProformaInvoice") : "");
				logBillingDepo.setTransactionID(jrs.getString("TransactionID") != null ? jrs.getString("TransactionID") : "");
				logBillingDepo.setAmount(jrs.getInt("Amount"));
				logBillingDepo.setInvoiceNumber(jrs.getString("InvoiceNumber") != null ? jrs.getString("InvoiceNumber") : "");
				logBillingDepo.setUrlInvoice(jrs.getString("UrlInvoice") != null ? jrs.getString("UrlInvoice") : "");
				logBillingDepo.setUrleFaktur(jrs.getString("UrleFaktur") != null ? jrs.getString("UrleFaktur") : "");
				logBillingDepo.setUrlBonMuat(jrs.getString("UrlBonMuat") != null ? jrs.getString("UrlBonMuat") : "");
				logBillingDepo.setBonMuatId(jrs.getString("BonMuatId") != null ? jrs.getString("BonMuatId") : "");
				logBillingDepo.setMessage(jrs.getString("Message") != null ? jrs.getString("Message") : "");
				logBillingDepo.setPaymentID(jrs.getString("PaymentID") != null ? jrs.getString("PaymentID") : "");
				logBillingDepo.setJournalNo(jrs.getString("JournalNo") != null ? jrs.getString("JournalNo") : "");
				logBillingDepo.setJurnalDate(jrs.getString("JurnalDate") != null ? jrs.getString("JurnalDate") : null);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "findLogBillingDepoByOrderID", sql , user);
		}

		return logBillingDepo;
	}
	
	public static mdlLogBillingDepo findLogBillingDepoByVendorOrderDetailID(String vendorOrderDetailID, String user) {
		mdlLogBillingDepo logBillingDepo = new mdlLogBillingDepo();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_GetLogBillingDepoByVendorOrderDetailID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderDetailID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "findLogBillingDepoByVendorOrderDetailID");

			while(jrs.next()){
				System.out.println(sql);
				logBillingDepo.setOrderManagementID(jrs.getString("OrderManagementID"));
				logBillingDepo.setBillingStatus(jrs.getInt("BillingStatus"));
				logBillingDepo.setNote(jrs.getString("Note") != null ? jrs.getString("Note") : "");
				logBillingDepo.setUrlProformaInvoice(jrs.getString("UrlProformaInvoice") != null ? jrs.getString("UrlProformaInvoice") : "");
				logBillingDepo.setTransactionID(jrs.getString("TransactionID") != null ? jrs.getString("TransactionID") : "");
				logBillingDepo.setAmount(jrs.getInt("Amount"));
				logBillingDepo.setInvoiceNumber(jrs.getString("InvoiceNumber") != null ? jrs.getString("InvoiceNumber") : "");
				logBillingDepo.setUrlInvoice(jrs.getString("UrlInvoice") != null ? jrs.getString("UrlInvoice") : "");
				logBillingDepo.setUrleFaktur(jrs.getString("UrleFaktur") != null ? jrs.getString("UrleFaktur") : "");
				logBillingDepo.setUrlBonMuat(jrs.getString("UrlBonMuat") != null ? jrs.getString("UrlBonMuat") : "");
				logBillingDepo.setBonMuatId(jrs.getString("BonMuatId") != null ? jrs.getString("BonMuatId") : "");
				logBillingDepo.setMessage(jrs.getString("Message") != null ? jrs.getString("Message") : "");
				logBillingDepo.setPaymentID(jrs.getString("PaymentID") != null ? jrs.getString("PaymentID") : "");
				logBillingDepo.setJournalNo(jrs.getString("JournalNo") != null ? jrs.getString("JournalNo") : "");
				logBillingDepo.setJurnalDate(jrs.getString("JurnalDate") != null ? jrs.getString("JurnalDate") : null);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "findLogBillingDepoByVendorOrderDetailID", sql , user);
		}

		return logBillingDepo;
	}
	
	public static String InsertLogBillingDepo(mdlLogBillingDepo param)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			
			sql = "{call sp_InserLogBillingDepo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getOrderManagementID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.getBillingStatus()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getNote()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrlProformaInvoice()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getTransactionID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.getAmount()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getInvoiceNumber()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrlInvoice()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrleFaktur()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrlBonMuat()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getBonMuatId()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getMessage()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getPaymentID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getJournalNo()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getJurnalDate()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertLogBillingDepo");

			result = success == true ? "Success Insert Log Billing Depo" : "Database Error";
		}
		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "InsertLogBillingDepo", sql);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateLogBillingDepo(mdlLogBillingDepo param)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateLogBillingDepo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getOrderManagementID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.getBillingStatus()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getNote()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrlProformaInvoice()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getTransactionID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.getAmount()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getInvoiceNumber()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrlInvoice()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrleFaktur()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getUrlBonMuat()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getBonMuatId()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getMessage()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getPaymentID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getJournalNo()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getJurnalDate()));


			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateLogBillingDepo");

			result = success == true ? "Success Update Log Billing Depo" : "Database Error";
		}
		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "UpdateLogBillingDepo", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String DeleteLogBillingDepoByOrderID(String orderID)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteDepo(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteLogBillingDepoByOrderID");

			result = success == true ? "Success Log Billing Depo" : "Database Error";
		}
		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "DeleteLogBillingDepoByOrderID", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	
}
