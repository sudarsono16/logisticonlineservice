package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;

public class KojaAdapter {

	public static modelKojaGetCms.mdlGetCms LoadKojaCmsData(String vodtID, String driverID) {
		modelKojaGetCms.mdlGetCms cmsData = new modelKojaGetCms.mdlGetCms();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_VendorOrderDetailLoadByDetailID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vodtID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadKojaCmsData");
			
			while(jrs.next()){
				cmsData.cmsLink = jrs.getString("Tps_CmsLink");
				/*cmsData.printCmsDateTime = jrs.getString("Tps_CmsDateTime");
				cmsData.curOwner = jrs.getString("Tps_CurOwner");
				cmsData.containerStatus = jrs.getString("Tps_ContainerStatus");
				cmsData.containerSize = jrs.getString("Tps_ContainerSize");
				cmsData.containerType = jrs.getString("Tps_ContainerType");
				cmsData.grossWeightDocumented = jrs.getInt("Tps_GrossWeightDocumented");
				cmsData.locationStuffing = jrs.getString("Tps_LocationStuffing");
				cmsData.tidNumber = jrs.getString("TID_Number");
				cmsData.vehicleNumber = jrs.getString("VehicleNumber");
				cmsData.containerNumber = jrs.getString("ContainerNumber");*/
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadKojaCmsData", sql , driverID);
		}
		return cmsData;
	}
	
	public static modelKojaGetEir.mdlGetEir LoadKojaEirData(String vodtID, String driverID) {
		modelKojaGetEir.mdlGetEir eirData = new modelKojaGetEir.mdlGetEir();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_VendorOrderDetailLoadByDetailID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vodtID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadKojaEirData");
			
			while(jrs.next()){
				eirData.eirLink = jrs.getString("Tps_EirLink");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadKojaEirData", sql , driverID);
		}
		return eirData;
	}
	
}
