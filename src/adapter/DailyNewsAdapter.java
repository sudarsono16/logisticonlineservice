package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlQueryExecute;

public class DailyNewsAdapter {
	public static List<model.mdlDailyNews> GetDailyNews(String driverID){
		List<model.mdlDailyNews> listDailyNews = new ArrayList<model.mdlDailyNews>();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
			sql = "{call sp_DailyNewsLoadByDriverID(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", driverID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_GetDailyNews");
			while(jrs.next()){
				model.mdlDailyNews mdlDailyNews = new model.mdlDailyNews();
				mdlDailyNews.newsID = jrs.getString("NewsID");
				mdlDailyNews.vendorID = jrs.getString("VendorID");
				mdlDailyNews.newsText = jrs.getString("NewsText");
				mdlDailyNews.newsImage = jrs.getString("NewsImage");
				listDailyNews.add(mdlDailyNews);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_GetDailyNews", sql , driverID);
		}
		return listDailyNews;
	}
}
