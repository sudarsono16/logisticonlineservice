package model;

public class mdlSettingInvoice {
	
	public Integer finalRate;
	public Integer customClearance;
	public Integer ppn;
	public Integer pph23;
	public Integer truckingAmount;
	
	public Integer getFinalRate() {
		return finalRate;
	}
	public void setFinalRate(Integer finalRate) {
		this.finalRate = finalRate;
	}
	public Integer getCustomClearance() {
		return customClearance;
	}
	public void setCustomClearance(Integer customClearance) {
		this.customClearance = customClearance;
	}
	public Integer getPpn() {
		return ppn;
	}
	public void setPpn(Integer ppn) {
		this.ppn = ppn;
	}
	public Integer getPph23() {
		return pph23;
	}
	public void setPph23(Integer pph23) {
		this.pph23 = pph23;
	}
	public Integer getTruckingAmount() {
		return truckingAmount;
	}
	public void setTruckingAmount(Integer truckingAmount) {
		this.truckingAmount = truckingAmount;
	}
	
	

}
