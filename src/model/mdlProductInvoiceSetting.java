package model;

public class mdlProductInvoiceSetting {
	
	private String productInvoice;
	
	private String customerName;

	public String getProductInvoice() {
		return productInvoice;
	}

	public void setProductInvoice(String productInvoice) {
		this.productInvoice = productInvoice;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
}
