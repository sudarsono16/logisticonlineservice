package model;

public class mdlVendorOrderDetail {
	public String vendorOrderDetailID;
	public String vendorOrderID;
	public String vehicleNumber;
	public String vendorDetailStatus;
	public String containerNumber;
	public String sealNumber;
	public String driverID;
	public String driverName;
	public String vendorID;
	public String vendorName;
	public String stuffingDate;
	public String tierID;
	public String tierDescription;
	public String containerTypeID;
	public String containerTypeDescription;
	public String itemDescription;
	public String factoryID;
	public String factoryName;
	public String factoryPic;
	public String factoryAddress;
	public String factoryOfficePhone;
	public String factoryMobilePhone;
	public String portID;
	public String portName;
	public String portUTC;
	public String shippingID;
	public String shippingName;
	public String shippingAddress;
	public String depoID;
	public String depoName;
	public String depoAddress;
	public String customerID;
	public String customerName;
	public String pic1;
	public String pic2;
	public String pic3;
	public String address;
	public String officePhone;
	public String mobilePhone;
	public String districtID;
	public String districtName;
	public String orderType;
	public String doShippingLine;
	public String shippingInstruction;
	public String peb;
	public String npe;
	public String yellowCard;
	public Integer party;
	public String depoIn;
	public String depoOut;
	public String factoryIn;
	public String factoryOut;
	public String portIn;
	public String portOut;
	public String orderManagementID;
	public String updatedBy;
	
	//tps gatepass
	public String isoCode;
	public Integer weight;
	public String transactionType;
	public String vessel;
	public String voyage;
	public String etb;
	public String closing;
	public String pod1;
	public String pod2;
	public String companyName;
	public String imoCode;
	public String customDoc;
	public String bat;
	public String invoiceNo;
	public String batDate;
	public String contact;
	public String contactName;
	public String tidNumber;
	public String owner;
	public String shipperName;
	
	public String getVendorOrderDetailID() {
		return vendorOrderDetailID;
	}
	public void setVendorOrderDetailID(String vendorOrderDetailID) {
		this.vendorOrderDetailID = vendorOrderDetailID;
	}
	public String getVendorOrderID() {
		return vendorOrderID;
	}
	public void setVendorOrderID(String vendorOrderID) {
		this.vendorOrderID = vendorOrderID;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVendorDetailStatus() {
		return vendorDetailStatus;
	}
	public void setVendorDetailStatus(String vendorDetailStatus) {
		this.vendorDetailStatus = vendorDetailStatus;
	}
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getSealNumber() {
		return sealNumber;
	}
	public void setSealNumber(String sealNumber) {
		this.sealNumber = sealNumber;
	}
	public String getDriverID() {
		return driverID;
	}
	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getTierID() {
		return tierID;
	}
	public void setTierID(String tierID) {
		this.tierID = tierID;
	}
	public String getTierDescription() {
		return tierDescription;
	}
	public void setTierDescription(String tierDescription) {
		this.tierDescription = tierDescription;
	}
	public String getContainerTypeID() {
		return containerTypeID;
	}
	public void setContainerTypeID(String containerTypeID) {
		this.containerTypeID = containerTypeID;
	}
	public String getContainerTypeDescription() {
		return containerTypeDescription;
	}
	public void setContainerTypeDescription(String containerTypeDescription) {
		this.containerTypeDescription = containerTypeDescription;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getFactoryID() {
		return factoryID;
	}
	public void setFactoryID(String factoryID) {
		this.factoryID = factoryID;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public String getFactoryPic() {
		return factoryPic;
	}
	public void setFactoryPic(String factoryPic) {
		this.factoryPic = factoryPic;
	}
	public String getFactoryAddress() {
		return factoryAddress;
	}
	public void setFactoryAddress(String factoryAddress) {
		this.factoryAddress = factoryAddress;
	}
	public String getFactoryOfficePhone() {
		return factoryOfficePhone;
	}
	public void setFactoryOfficePhone(String factoryOfficePhone) {
		this.factoryOfficePhone = factoryOfficePhone;
	}
	public String getFactoryMobilePhone() {
		return factoryMobilePhone;
	}
	public void setFactoryMobilePhone(String factoryMobilePhone) {
		this.factoryMobilePhone = factoryMobilePhone;
	}
	public String getPortID() {
		return portID;
	}
	public void setPortID(String portID) {
		this.portID = portID;
	}
	public String getPortName() {
		return portName;
	}
	public void setPortName(String portName) {
		this.portName = portName;
	}
	public String getPortUTC() {
		return portUTC;
	}
	public void setPortUTC(String portUTC) {
		this.portUTC = portUTC;
	}
	public String getShippingID() {
		return shippingID;
	}
	public void setShippingID(String shippingID) {
		this.shippingID = shippingID;
	}
	public String getShippingName() {
		return shippingName;
	}
	public void setShippingName(String shippingName) {
		this.shippingName = shippingName;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getDepoID() {
		return depoID;
	}
	public void setDepoID(String depoID) {
		this.depoID = depoID;
	}
	public String getDepoName() {
		return depoName;
	}
	public void setDepoName(String depoName) {
		this.depoName = depoName;
	}
	public String getDepoAddress() {
		return depoAddress;
	}
	public void setDepoAddress(String depoAddress) {
		this.depoAddress = depoAddress;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPic1() {
		return pic1;
	}
	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}
	public String getPic2() {
		return pic2;
	}
	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}
	public String getPic3() {
		return pic3;
	}
	public void setPic3(String pic3) {
		this.pic3 = pic3;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getDistrictID() {
		return districtID;
	}
	public void setDistrictID(String districtID) {
		this.districtID = districtID;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getDoShippingLine() {
		return doShippingLine;
	}
	public void setDoShippingLine(String doShippingLine) {
		this.doShippingLine = doShippingLine;
	}
	public String getShippingInstruction() {
		return shippingInstruction;
	}
	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}
	public String getPeb() {
		return peb;
	}
	public void setPeb(String peb) {
		this.peb = peb;
	}
	public String getNpe() {
		return npe;
	}
	public void setNpe(String npe) {
		this.npe = npe;
	}
	public String getYellowCard() {
		return yellowCard;
	}
	public void setYellowCard(String yellowCard) {
		this.yellowCard = yellowCard;
	}
	public Integer getParty() {
		return party;
	}
	public void setParty(Integer party) {
		this.party = party;
	}
	public String getDepoIn() {
		return depoIn;
	}
	public void setDepoIn(String depoIn) {
		this.depoIn = depoIn;
	}
	public String getDepoOut() {
		return depoOut;
	}
	public void setDepoOut(String depoOut) {
		this.depoOut = depoOut;
	}
	public String getFactoryIn() {
		return factoryIn;
	}
	public void setFactoryIn(String factoryIn) {
		this.factoryIn = factoryIn;
	}
	public String getFactoryOut() {
		return factoryOut;
	}
	public void setFactoryOut(String factoryOut) {
		this.factoryOut = factoryOut;
	}
	public String getPortIn() {
		return portIn;
	}
	public void setPortIn(String portIn) {
		this.portIn = portIn;
	}
	public String getPortOut() {
		return portOut;
	}
	public void setPortOut(String portOut) {
		this.portOut = portOut;
	}
	
	//get set tps gatepass

	public String getIsoCode() {
		return isoCode;
	}
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getVessel() {
		return vessel;
	}
	public void setVessel(String vessel) {
		this.vessel = vessel;
	}
	public String getVoyage() {
		return voyage;
	}
	public void setVoyage(String voyage) {
		this.voyage = voyage;
	}
	public String getEtb() {
		return etb;
	}
	public void setEtb(String etb) {
		this.etb = etb;
	}
	public String getClosing() {
		return closing;
	}
	public void setClosing(String closing) {
		this.closing = closing;
	}
	public String getPod1() {
		return pod1;
	}
	public void setPod1(String pod1) {
		this.pod1 = pod1;
	}
	public String getPod2() {
		return pod2;
	}
	public void setPod2(String pod2) {
		this.pod2 = pod2;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getImoCode() {
		return imoCode;
	}
	public void setImoCode(String imoCode) {
		this.imoCode = imoCode;
	}
	public String getCustomDoc() {
		return customDoc;
	}
	public void setCustomDoc(String customDoc) {
		this.customDoc = customDoc;
	}
	public String getBat() {
		return bat;
	}
	public void setBat(String bat) {
		this.bat = bat;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getBatDate() {
		return batDate;
	}
	public void setBatDate(String batDate) {
		this.batDate = batDate;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getTidNumber() {
		return tidNumber;
	}
	public void setTidNumber(String tidNumber) {
		this.tidNumber = tidNumber;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getOrderManagementID() {
		return orderManagementID;
	}
	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getShipperName() {
		return shipperName;
	}
	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
