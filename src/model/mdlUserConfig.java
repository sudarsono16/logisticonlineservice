package model;

public class mdlUserConfig {
	public String driverID;
	public String driverName;
	public String vendorID;
	public String vendorName;
	public String ipLocal;
	public String portLocal;
	public String ipPublic;
	public String portPublic;
	public String ipAlternative;
	public String portAlternative;
	public String password;
}
