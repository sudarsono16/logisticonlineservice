package model;

public class mdlDriverImage {

	public String vendorOrderDetailID;
    public String date;
    public String driverID;
    public String driverName;
    public String path;
    public String type;
    
    
    public String getVendorOrderDetailID() {
		return vendorOrderDetailID;
	}
	public void setVendorOrderDetailID(String vendorOrderDetailID) {
		this.vendorOrderDetailID = vendorOrderDetailID;
	}
	public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getDriverID() {
        return driverID;
    }
    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }
    public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    
}
