package model;

public class mdlCustomerEmail {
	public String shippingInstruction;
	public String doShippingLine;
	public Integer totalQuantity;
	public Double totalPrice;
	public String tier;
	public String port;
	public String shipping;
	public String depo;
	public String vendorOrderDetailID;
	public String driverID;
	public String driverName;
	public String vehicleNumber;
	public String orderType;
	public String itemDescription;
	public String customerID;
	public String customerName;
	public String email;
	public String orderManagementID;
	public String orderManagementDetailID;
	public String factoryName;
	public String partyDescription;
	public String route;
	
	public mdlCustomerEmail() {
		super();
	}

	public mdlCustomerEmail(String shippingInstruction, String doShippingLine, Integer totalQuantity, Double totalPrice,
			String tier, String port, String shipping, String depo, String vendorOrderDetailID, String driverID,
			String driverName, String vehicleNumber, String orderType, String itemDescription, String customerID,
			String customerName, String email, String orderManagementID, String factoryName, String partyDescription,
			String route) {
		super();
		this.shippingInstruction = shippingInstruction;
		this.doShippingLine = doShippingLine;
		this.totalQuantity = totalQuantity;
		this.totalPrice = totalPrice;
		this.tier = tier;
		this.port = port;
		this.shipping = shipping;
		this.depo = depo;
		this.vendorOrderDetailID = vendorOrderDetailID;
		this.driverID = driverID;
		this.driverName = driverName;
		this.vehicleNumber = vehicleNumber;
		this.orderType = orderType;
		this.itemDescription = itemDescription;
		this.customerID = customerID;
		this.customerName = customerName;
		this.email = email;
		this.orderManagementID = orderManagementID;
		this.factoryName = factoryName;
		this.partyDescription = partyDescription;
		this.route = route;
	}

	public String getShippingInstruction() {
		return shippingInstruction;
	}

	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}

	public String getDoShippingLine() {
		return doShippingLine;
	}

	public void setDoShippingLine(String doShippingLine) {
		this.doShippingLine = doShippingLine;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getShipping() {
		return shipping;
	}

	public void setShipping(String shipping) {
		this.shipping = shipping;
	}

	public String getDepo() {
		return depo;
	}

	public void setDepo(String depo) {
		this.depo = depo;
	}

	public String getVendorOrderDetailID() {
		return vendorOrderDetailID;
	}

	public void setVendorOrderDetailID(String vendorOrderDetailID) {
		this.vendorOrderDetailID = vendorOrderDetailID;
	}

	public String getDriverID() {
		return driverID;
	}

	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrderManagementID() {
		return orderManagementID;
	}

	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}

	public String getFactoryName() {
		return factoryName;
	}

	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}

	public String getPartyDescription() {
		return partyDescription;
	}

	public void setPartyDescription(String partyDescription) {
		this.partyDescription = partyDescription;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}
	
	
	
}
